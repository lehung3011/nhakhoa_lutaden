@php
    use App\Helpers\URL;
@endphp
@isset($itemFaqs)
    @php $i = 0; @endphp
    <div class="accrodion-grp faq-accrodion" data-grp-name="faq-accrodion">
    @foreach ($itemFaqs as $item)
        @php
            $i++;
            $question   = $item['question'];
            $answer     = $item['answer'];
            $class = ($i== 1) ? "active" : '';
        @endphp
        <div class="accrodion {{$class}}">
            <div class="accrodion-title">
                <h4>
                    <span class="decor"><span class="inner"></span></span>
                    <span class="text">{{ $question }}</span>
                </h4>
            </div>
            <div class="accrodion-content" style="display: none;">
                {!! $answer !!}
            </div>
        </div>
    @endforeach
    </div>
@endisset





{{--  <div class="accrodion-grp faq-accrodion" data-grp-name="faq-accrodion">                                                                                                                                   <div class="accrodion active">
    <div class="accrodion-title">
        <h4>
        <span class="decor"><span class="inner"></span></span>
        <span class="text">Tái tạo răng bằng cách bọc mão là gì?</span>
        </h4>
    </div>
    <div class="accrodion-content" style="display: block;">
        <p>Phương pháp này được sử dụng trong trường hợp răng bị vỡ lớn, răng chết tủy hoặc các kỹ thuật khác không đáp ứng được đòi hỏi điều trị. Mão răng có 3 loại: kim loại, composite và sứ.</p>
        <p>Mão sứ là phục hình có tính thẩm mỹ và độ bền cao nhất. Có thể sử dụng từ 10 đến 20 năm hoặc hơn nữa. Mão sứ ngày càng được ưa chuộng so với mão composite và kim loại do tính thẫm mỹ cao và độ bền của nó.</p>
    </div>
</div>  --}}
