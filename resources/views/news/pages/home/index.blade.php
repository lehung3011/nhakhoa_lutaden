@extends('news.main')
@section('content')
    @include ('news.block.slider')
    @include ('news.pages.home.child-index.info_work')
    @include ('news.pages.home.child-index.news_promotion')
    @include ('news.pages.home.child-index.testimonial')
    @include ('news.pages.home.child-index.our_department')
    @include ('news.block.lastest_news')
@endsection