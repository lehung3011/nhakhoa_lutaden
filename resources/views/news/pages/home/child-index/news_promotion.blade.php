@php
    use App\Helpers\Template as Template;
    use App\Helpers\URL;
    use Illuminate\Support\Str;
@endphp
@isset($itemsPromotion)
<section class="sec-pdd-90 zvn-pd">
    <div class="container">
        <div class="sec-title text-center">
            <h2>Thông tin ưu đãi</h2>
            <span class="decor"><span class="inner"></span></span>
        </div>
        <div class="row">
            @foreach($itemsPromotion as $item) 
                @php
                    $name         = $item['name'];
                    $thumb        = asset($item['thumb']);
                    $short        = Str::limit(strip_tags($item['content'],''), 120, ' ...');
                    $linkArticle  = URL::linkArticle($item['id'], $item['name']);
                @endphp
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-specialize text-center">
                        <div class="image-box">
                            <img src="{{$thumb}}" alt="{{$name}}"/>
                        </div>
                        <div class="content">
                            <h3>{{$name}}</h3>
                            <p>{!!$short!!}</p>
                            <a href="{{$linkArticle}}" class="thm-btn">Xem chi tiết</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endisset