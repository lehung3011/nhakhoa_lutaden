@php
use App\Helpers\Template as Template;
use App\Helpers\URL;
    $list_nav = '<ul class="nav nav-pills nav-justified">';
        $i = 0;
    foreach($itemsAgencies as $district => $item){
        $active_nav  = ($i == 0) ? ' class="active"' : '';
        $list_nav .= sprintf('<li %s><a data-toggle="pill" href="#menu%s">%s</a></li>', $active_nav, $i, $district);
        $i++;
    }
    $list_nav .= '</ul>';
@endphp

@isset($itemsAgencies)
<div class="department-section sec-pdd-90 zvn-pd">
    <div class="container">
        <div class="sec-title text-center">
            <h2>Hệ thống chi nhánh</h2>
            <span class="decor"><span class="inner"></span></span>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 company">
                <div class="company-tab">
                    {!! $list_nav !!}
                    <div class="tab-content">
                        @php $i = 0; @endphp
                        @foreach($itemsAgencies as $district => $items)
                        @php
                            $active  = ($i == 0) ? ' active' : '';
                        @endphp
                        <div id="menu{{$i}}" class="tab-pane fade in {{$active}}">
                            <div class="row">
                                @foreach ($items as $item)
                                @php
                                    $image          = asset($item['image']);
                                    $name           = $item['name'];
                                    $address        = $item['address'];
                                    $linkAgencies   = route('agencies');
                                @endphp
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="single-specialize text-center">
                                        <div class="image-box">
                                            <img src="{{ $image }}" alt="{{ $name }}"/>
                                        </div>
                                        <div class="content">
                                            <h3>{{$name}}</h3>
                                            <p>{{$address}}</p>
                                            <a href="{{ $linkAgencies }}" class="thm-btn">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @php $i++; @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endisset