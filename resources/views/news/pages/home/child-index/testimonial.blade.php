@php
    use App\Helpers\Template as Template;
    use App\Helpers\URL;
@endphp

<section class="sec-pdd-90-85 testimonial-section testimonials-wrapper zvn-pd">
    <div class="container">
        <div class="sec-title colored text-center">
            <h2>Trải nghiệm khách hàng</h2>
            <span class="decor">
                <span class="inner"></span>
            </span>
        </div>

        <!--Three Item Carousel-->
        <div class="three-item-carousel">
            @foreach($itemsComment as $item) 
            @php
                $name         = $item['guest_name'];
                $thumb        = asset($item['image']);
                $comment      = $item['comment'];
            @endphp
            <div class="testimonial-block-one">
                <div class="inner-box zvn-panel-wrapper">
                    <div class="quote-icon">
                        <span class="icon flaticon-left-quote"></span>
                    </div>
                    <div class="author-info">
                        <div class="author-image">
                            <img src="{{$thumb}}" alt="{{$name}}"/>
                        </div>
                        <h3>{{ $name }}</h3>
                        <div class="designation">Execfise</div>
                        <!--Rating-->
                        <div class="rating">
                            <span class="fa fa-star"></span
                            ><span class="fa fa-star"></span
                            ><span class="fa fa-star"></span
                            ><span class="fa fa-star"></span
                            ><span class="fa fa-star"></span>
                        </div>
                    </div>

                    <div class="text zvn-panel">
                        {!!$comment!!}
                        <span class="zvn-dots">...</span>
                        <span class="zvn-more"></span>
                    </div>

                    <button class="zvn-read-more">Xem thêm</button>

                    <!--Author Info-->
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>