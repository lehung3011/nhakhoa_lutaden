@php
use App\Helpers\Template as Template;
use App\Helpers\URL;
use Illuminate\Support\Str;
@endphp

@foreach($item['articles']  as $article)
    @php
    $name         = $article['name'];
    $thumb        = asset($article['thumb']);
    $class = "";
    if(!empty($type) && $type == "single") {
        $class = "img-fluid w-100";
    }
    $linkArticle  = URL::linkArticle($article['id'], $article['name']);
    $created      = Template::showDatetimeFrontend($article['created']);
    $content        = Str::limit(strip_tags($article['content'],''), 200, ' ...');
    @endphp
<div class="single-blog-post">
    <div class="img-box">
        <img src="{{  $thumb  }}" alt="{{ $name }}" class="{{ $class }}">
    </div>
    <div class="content-box clearfix">
        <div class="date-box">
            <div class="inner">
                <div class="date">
                    <b>{{@date('d', $article['created'])}}</b>
                    <strong>Tháng</strong>
                    <b>{{@date('m', $article['created'])}}</b>
                </div>
            </div>
        </div>
        <div class="content">
            <h3><a href="{{ $linkArticle }}">{{ $name }}</a></h3>
            <p>{{ $content }}</p>
        </div>
    </div>
</div>
@endforeach
