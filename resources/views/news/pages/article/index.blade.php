@php
use Illuminate\Support\Facades\URL;
@endphp
@extends('news.main')

@push('extra_js')
<script src="{{asset('news/js/article.js')}}"></script>
<script type="text/javascript">
	var cat_id = '{{$itemArticle['category_id']}}';
	var article_id = '{{$itemArticle['id']}}';
</script>
@endpush
@section('content')
<div class="section-category">
    @include('news.block.breadcrumb_article', ['item' => $itemArticle])
    <div class="blog-home sec-pdd-90 blog-page blog-details zvn-blog-details ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 pull-left">
                    <div class="single-blog-post">
                        @include('news.pages.article.child-index.article', ['item' => $itemArticle]) 
                        <div class="facebook-comment">
                            <div class="fb-comments" data-href="{{ URL::current() }}" data-width="100%" data-numposts="5"></div>	
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 pull-right">
                    <div class="side-bar-widget itm-mgn-top-50">
                        <!-- Latest Posts -->
                        <div id="article_latest_posts"></div>
                    </div>
                </div>
            </div>
            <div id="article_related"></div>
        </div>
    </div>
</div>
@endsection