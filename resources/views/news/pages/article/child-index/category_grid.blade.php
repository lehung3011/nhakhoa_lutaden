@php
use App\Helpers\Template as Template;
use App\Helpers\URL;
use Illuminate\Support\Str;
@endphp

@foreach($related_articles  as $article)
    @php
        $name         = $article['name'];
        $thumb        = asset($article['thumb']);

        $linkArticle  = URL::linkArticle($article['id'], $article['name']);
        $created      = Template::showDatetimeFrontend($article['created']);
    @endphp
    <div class="item">
        <div class="single-team-member">
            <div class="img-box">
                <img src="{{$thumb}}" alt="{{$name}}" style="height: 150px">
            </div>
            <a href="{{$linkArticle}}">
                <h3>{{$name}}</h3>
            </a>
            <small class="zvn-posted-time">Ngày đăng: {{$created}}</small>
        </div>
    </div>
@endforeach