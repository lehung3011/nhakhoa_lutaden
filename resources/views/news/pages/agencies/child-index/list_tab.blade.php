@php
    use App\Helpers\URL;
@endphp
@isset($itemGroupDistrict)
    <div class="company-tab">
        <ul class="nav nav-pills nav-justified">
            <li class="active"><a class="zvn-get-agency" data-branch="Tất cả" data-toggle="pill" href="#menu0">Tất cả</a></li>
    @php $i = 0; @endphp
    @foreach ($itemGroupDistrict as $item)
        @php
            $i++;
            $listDistrict = config('zvn.template.district');
            $district = $item['district'];
        @endphp
        @if (array_key_exists($district,array_flip($listDistrict)))
            <li class=""><a class="zvn-get-agency" data-branch="{{$district}}" data-toggle="pill" href="#menu{{$i}}">{{$district}}</a></li>
        @endif
    @endforeach
        </ul>
    </div>
@endisset