@php
use App\Helpers\Template as Template;
use App\Helpers\URL;
use Illuminate\Support\Str;
@endphp
@isset($itemAgencies)
    @foreach ($itemAgencies as $item)
        @php
            $name         = $item['name'];
            $thumb        = asset($item['image']);
            $description  = $item['description'];
            $address      = $item['address'];
            $hotline      = $item['hotline'];
            $arr_hotline  = explode(',',$hotline);
            $map          = $item['map'];
        @endphp
        <div class="row zvn-row zvn-branch">
            <div class="col-lg-4 col-md-4">
                <div class="pormo_box_img">
                    <img src="{{ $thumb }}" alt="">
                </div>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="col-lg-12 col-md-12">
                    <div class="pormo_box_details">
                        <h4><a href="" style="font-weight: bold">{{ $name }}</a></h4>
                        <p style="font-weight: bold"> <i class="fa fa-map-marker" aria-hidden="true"></i>{{$address}}</p>
                        {!!$description!!}
                    </div>
                    <div class="zvn-box-btn zvn-flex-col">
                        @foreach ($arr_hotline as $hotline)
                        <a href="tel:{{$hotline}}" class="zvn-button"><i class="fa fa-phone" aria-hidden="true"></i> {{$hotline}}</a>
                        @endforeach
                        <a href="{{ route('booking') }}" class="zvn-button"><i class="fa fa-calendar" aria-hidden="true"></i> Đặt lịch hẹn</a>
                        <a href="{{$map}}" target="_blank" class="zvn-button"><i class="fa fa-location-arrow" aria-hidden="true"></i> Xem chỉ đường</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset