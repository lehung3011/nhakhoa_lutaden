@extends('news.main')
@section('content')
@include('news.block.breadcrumb', ['item' => ['name' => 'Hệ thống chi nhánh']])
<div class="sec-pdd-20 contact-content appointment-content">
    <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 company">
                    @include('news.pages.agencies.child-index.list_tab') 
                    @include('news.pages.agencies.child-index.list_agencies') 
                </div>
            </div>
        </div>
</div>
@endsection

@push('extra_js')
<script>
    $(document).ready(function () {
        $('.zvn-get-agency').click(function () {
            var type=$(this).data('branch');
            var elmBranch = $('.zvn-branch');
            var elmTabBranch = $('.company-tab');
            var url = 'http://lutaden.xyz/info';
            var url_booking = "{{route('booking')}}";
            // $(this).parent().find('.zvn-button-branch').removeClass('zvn-active');
            // $(this).addClass('zvn-active');
            $.ajax({
                method:"GET",
                url: root + '/info',
                data:{type_district: type},
                dataType:'json',
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                success:function (response) {
                    if(response.code==1){
                        elmBranch.remove();
                        var data = response.data;
                        console.log(data);
                        var Xhtml='';
                        for (var i=0;i<data.length;i++)
                        {
                            var image = 'http://lutaden.xyz/public/'+data[i]['image'];
                            var name = data[i]['name'];
                            var address = data[i]['address'];
                            var map = data[i]['map'];
                            var description = data[i]['description'];
                            var hotline = data[i]['hotline'].split(',');
                            Xhtml += '<div class="row zvn-row zvn-branch">' +
                                        '<div class="col-lg-4 col-md-4">'   +
                                            '<div class="pormo_box_img">'   +
                                                '<img src="'+ image +'" alt="">'+
                                            '</div>'+
                                        '</div>'    +
                                        '<div class="col-lg-8 col-md-8">' +
                                            '<div class="col-lg-12 col-md-12">' +
                                                '<div class="pormo_box_details">' +
                                                    '<h4><a href="" style="font-weight: bold"> '+ name +'</a></h4>' +
                                                    '<p style="font-weight: bold"> <i class="fa fa-map-marker" aria-hidden="true"></i> '+ address +' </p>' +
                                                    description +
                                                '</div>' +
                                                '<div class="zvn-box-btn zvn-flex-col">';
                                                    for (var j=0;j< hotline.length;j++){
                                                        Xhtml +='<a href="tel:+84' +hotline[j]+'" class="zvn-button"><i class="fa fa-phone" aria-hidden="true"></i> '+hotline[j]+'</a>';
                                                    }
                            Xhtml += '<a href="'+ url_booking +'"  class="zvn-button"><i class="fa fa-calendar" aria-hidden="true"></i> Đặt lịch hẹn</a>';
                            if( map !== null && map.length>0 ) {
                                Xhtml += '<a href="' + map +'" target=_blank class="zvn-button"><i class="fa fa-location-arrow" aria-hidden="true"></i> Xem chỉ đường</a>';
                            }
                            Xhtml += ' </div></div></div></div>';
                        }
                        $(Xhtml).insertAfter(elmTabBranch);
                    }
                }
            })
        });
    });
</script>
@endpush

