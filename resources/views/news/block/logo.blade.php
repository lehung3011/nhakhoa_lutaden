@php
    $src_logo = get_json_setting('setting-main','logo');
@endphp
@isset($src_logo)
<div class="logo pull-left">
    <a href="{{ route('home') }}">
        <img src="{{ asset($src_logo) }}"  alt="Nha Khoa Lutadent"/>
    </a>
</div>
@endisset