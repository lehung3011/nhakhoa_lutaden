@php
    use App\Helpers\Template as Template;
    use App\Helpers\URL;
@endphp
<div class="footer-widget quick-links">
    <h3 class="title">Dịch vụ</h3>
    <ul>
        @foreach($itemsService as $item) 
        @php
            $name         = $item['name'];
            $linkArticle  = URL::linkArticle($item['id'], $item['name']);
        @endphp
        <li>
            <a href="{{ $linkArticle }}">{{ $name }}</a>
        </li>
        @endforeach
    </ul>
</div>