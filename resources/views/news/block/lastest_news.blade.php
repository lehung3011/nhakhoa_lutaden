@php
    use App\Helpers\Template as Template;
    use App\Helpers\URL;
@endphp
@isset($itemsLatest)
<section class="blog-home sec-pdd-90 bg-news zvn-pd zvn-blog-news">
    <div class="container">
        <div class="sec-title text-center">
            <h2>Tin mới nhất</h2>
            <span class="decor">
                <span class="inner"></span>
            </span>
        </div>
        <div class="row">
            @foreach($itemsLatest as $item) 
                @php
                    $name         = $item['name'];
                    $thumb        = asset($item['thumb']);
                    $categoryName = $item['category_name'];
                    $linkCategory = URL::linkCategory($item['category_id'], $item['category_name']);;
                    $linkArticle  = URL::linkArticle($item['id'], $item['name']);
                    $created      = Template::showDatetimeFrontend($item['created']);
                @endphp
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-blog-post">
                    <div class="img-box">
                        <img src="{{ $thumb }}" alt="{{ $name }}" style="height: 155px;"/>
                    </div>
                    <div class="content-box">
                        <div class="content">
                            <a href="{{ $linkArticle }}"><h3>{{ $name }}</h3></a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endisset
