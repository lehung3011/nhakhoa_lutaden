<!-- main jQuery -->
<script src="https://nhakhoalutadent.com/dentist/js/jquery-1.11.1.min.js"></script>
<!-- bootstrap -->
<script src="https://nhakhoalutadent.com/dentist/js/bootstrap.min.js"></script>
<!-- bx slider -->
<script src="https://nhakhoalutadent.com/dentist/js/jquery.bxslider.min.js"></script>
<!-- appear js -->
<script src="https://nhakhoalutadent.com/dentist/js/jquery.appear.js"></script>
<!-- count to -->
<script src="https://nhakhoalutadent.com/dentist/js/jquery.countTo.js"></script>

<!-- mixit up -->
<script src="https://nhakhoalutadent.com/dentist/js/jquery.mixitup.min.js"></script>
<!-- wow -->
<script src="https://nhakhoalutadent.com/dentist/js/wow.js"></script>
<!-- fancybox -->
<script src="https://nhakhoalutadent.com/dentist/js/jquery.fancybox.pack.js"></script>
<!-- easing -->
<script src="https://nhakhoalutadent.com/dentist/js/jquery.easing.min.js"></script>
<script src="https://nhakhoalutadent.com/dentist/js/jquery.datetimepicker.js"></script>

<script src="https://nhakhoalutadent.com/dentist/js/isotope.pkgd.min.js"></script>

<script src="{{asset('news/js/jquery-ui/jquery-ui.js')}}"></script>

<script src="{{asset('news/js/slick/js/slick.min.js')}}"></script>
<script src="{{asset('news/js/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{asset('news/js/easing/easing.js')}}"></script>
<script src="{{asset('news/js/parallax-js-master/parallax.min.js')}}"></script>

<script src="{{asset('news/js/custom.js')}}"></script>
<script src="{{asset('news/js/myJS.js')}}"></script>
<script type="text/javascript">
    var root = '{{URL::to('/')}}';
</script>
@stack('extra_js')
