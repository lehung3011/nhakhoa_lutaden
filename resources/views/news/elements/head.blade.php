<title>@yield('title') - Lutadent</title>
<meta charset="UTF-8" />
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0"/>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta property="og:site_name" content="" />
<meta property="og:url" content="https://nhakhoalutadent.com" />
<meta property="og:image" itemprop="thumbnailUrl" content="" />
<meta property="og:title" itemprop="headline" content="" />
<meta property="og:type" content="website" />
<meta property="og:description" itemprop="description" content="" />
<meta name="csrf-token" content="{!! csrf_token() !!}">
<link rel="icon" href="/dentist/img/favicon.ico" type="image/ico" />
<link rel="stylesheet" type="text/css" href="{{asset('news/js/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('news/js/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('news/js/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('news/js/slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('news/js/slick/slick-theme.css')}}">
<link rel="stylesheet" href="{{asset('news/css/style.css')}}"/>
<link rel="stylesheet" href="{{asset('news/css/responsive.css')}}"/>
<link rel="stylesheet" href="{{asset('news/css/myCss.css')}}"/>
<link rel="stylesheet" href="{{asset('news/css/jquery.datetimepicker.css')}}"/>
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap&subset=vietnamese" rel="stylesheet"/>
@stack('extra_css')