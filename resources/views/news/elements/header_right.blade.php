@php
    $hotline = get_json_setting('setting-main','hotline');
@endphp
<div class="header-right-info pull-right clearfix hidden-xs">
    <div class="zvn-btn-call">
        @isset($hotline)
        <a href="javascript::void()" data-toggle="modal"  data-target="#myModal" class="thm-btn zvn-call hidden-sm hidden-xs">
            <i class="fa fa-phone" aria-hidden="true"></i>
            {{$hotline}}
        </a>    
        @endisset
        <a href="{{ route('booking')}}" target="_blank" class="thm-btn zvn-chat hidden-sm hidden-xs">Đặt lịch hẹn</a>
    </div>
    @include('news.block.request_call_back')
</div>