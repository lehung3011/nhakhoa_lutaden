@php
    $name         = $item['name'];
    $thumb        = asset($item['thumb']);
    $class = "";
    if(!empty($type) && $type == "single") {
        $class = "img-fluid w-100";
    }
@endphp
<div class="img-box">
    <img src="{{  $thumb  }}" alt="{{ $name }}" class="{{ $class }}">
</div>
