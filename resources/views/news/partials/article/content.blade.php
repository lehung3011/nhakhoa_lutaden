@php
    use App\Helpers\Template as Template;
    use App\Helpers\URL;
    use Illuminate\Support\Str;
    
    $name         = $item['name'];
    $thumb        = (!strstr($item['thumb'],'http://') && !strstr($item['thumb'],'https://')) ? asset('images/article/' . $item['thumb']) : $item['thumb'];
   

    if ($showCategory) {
        $linkCategory  =  URL::linkCategory($item['category_id'], $item['category_name']);
    }

    $linkArticle  = URL::linkArticle($item['id'], $item['name']);
    $created      = Template::showDatetimeFrontend($item['created']);

    if($lenghtContent === "full") {
        $content      = $item['content'];
    }else {
        $content        = Str::limit(strip_tags($item['content'],''), $lenghtContent, ' ...');
    }
@endphp
<div class="content-box clearfix">

    @if ($lenghtContent > 0)
        <div class="content zvn-content">
            <p>{{ $content }}</p>
        </div>
    @endif

    @if ($lenghtContent == "full")
        <div class="content zvn-content">
            <div class="zvn-content-text">
                {!! $content !!}
            </div>
            <div class="bottom-box clearfix">
                <div class="fb-share-button fb_iframe_widget" data-href="https://nhakhoalutadent.com/bai-viet/gioi-thieu-chung-48.html" data-layout="button_count" data-size="large" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=777&amp;href=https%3A%2F%2Fnhakhoalutadent.com%2Fbai-viet%2Fgioi-thieu-chung-48.html&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;size=large"><span style="vertical-align: bottom; width: 98px; height: 28px;"><iframe name="f2ce28d39de9c" width="1000px" height="1000px" data-testid="fb:share_button Facebook Social Plugin" title="fb:share_button Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v3.3/plugins/share_button.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df1a16425970beb8%26domain%3Dnhakhoalutadent.com%26origin%3Dhttps%253A%252F%252Fnhakhoalutadent.com%252Ff19b475719d88f8%26relation%3Dparent.parent&amp;container_width=777&amp;href=https%3A%2F%2Fnhakhoalutadent.com%2Fbai-viet%2Fgioi-thieu-chung-48.html&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;size=large" style="border: none; visibility: visible; width: 98px; height: 28px;" class=""></iframe></span></div>
            </div>
        </div>
    @endif
</div>

