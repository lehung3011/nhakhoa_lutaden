@php
    use App\Helpers\Template as Template;
    use App\Helpers\Hightlight as Hightlight;
@endphp
<div class="x_content">
    <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title">#</th>
                    <th class="column-title">Nội dung</th>
                    <th class="column-title">Hình ảnh</th>
                    <th class="column-title">Trạng thái</th>
                    <th class="column-title">Ngày tạo</th>
                    <th class="column-title">Chỉnh sửa</th>
                    <th class="column-title">Hành động</th>
                </tr>
            </thead>
            <tbody>
                @if (count($items) > 0)
                    @foreach ($items as $key => $val)
                        @php
                            $index           = $key + 1;
                            $class           = ($index % 2 == 0) ? "even" : "odd";
                            $id              = $val['id'];
                            $name            = Hightlight::show($val['guest_name'], $params['search'], 'name');
                            $content         = Hightlight::show($val['comment'], $params['search'], 'content');
                            $rate            = $val['rate'];
                            $thumb           = Template::showItemThumb($controllerName, $val['image'], $val['guest_name']);
                            $status          = Template::showItemStatus($controllerName, $id, $val['status']); 
                            $createdHistory = Template::showItemHistory($val['created_by'], $val['created']);
                            $modifiedHistory = Template::showItemHistory($val['modified_by'], $val['modified']);
                            $listBtnAction   = Template::showButtonAction($controllerName, $id);
                        @endphp

                        <tr class="{{ $class }} pointer">
                            <td width="5%">{{ $index }}</td>
                            <td width="30%">
                                <p><strong>Họ Tên : </strong>{!! $name !!}</p>
                                <p><strong>Đánh giá : </strong>{!! $rate !!}</p>
                                <p><strong>Nhận xét : </strong>{!! $content !!}</p>
                            </td>
                            <td width="5%">
                                <p>{!! $thumb !!}</p>
                            </td>
                            <td>{!! $status !!}</td>
                            <td>{!! $createdHistory !!}</td>
                            <td>{!! $modifiedHistory !!}</td>
                            <td class="last">{!! $listBtnAction !!}</td>
                        </tr>
                    @endforeach
                @else
                    @include('admin.templates.list_empty', ['colspan' => 6])
                @endif
            </tbody>
        </table>
    </div>
</div>
           