@extends('admin.main')
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');
    $formCkeditor  = config('zvn.template.form_ckeditor');
    $statusValue      = ['default' => 'Select status', 'active' => config('zvn.template.status.active.name'), 'inactive' => config('zvn.template.status.inactive.name')];

    $inputHiddenID    = Form::hidden('id', $item['id']);
    $inputHiddenThumb = Form::hidden('thumb_current', $item['thumb']); 

    $elements = [
        [
            'label'   => Form::label('name', 'Name', $formLabelAttr),
            'element' => Form::text('name', $item['name'],  $formInputAttr )
        ],[
            'label'   => Form::label('content', 'Content', $formLabelAttr),
            'element' => Form::textArea('content', $item['content'],  $formCkeditor )
        ],[
            'label'   => Form::label('thumb', 'Thumb', $formLabelAttr),
            'element' => FormTemplate::input_image('thumb', (!empty($item['id'])) ? $item['thumb'] : null, $formInputAttr),
            'thumb'   => (!empty($item['id'])) ? Template::showItemThumb($controllerName, $item['thumb'], $item['name']) : null ,
            'type'    => "thumb"
        ]
    ];
    
    $elements_cat = [
        [
            'label'   => null,
            'element' => Form::select('category_id', $itemsCategory, $item['category_id'],  $formInputAttr)
        ]
    ];
    
    $elements_status = [
        [
            'label'   => null,
            'element' => Form::select('status', $statusValue, $item['status'],  $formInputAttr)
        ]
    ];

    $elements_submit = [
        [
            'element' => $inputHiddenID . $inputHiddenThumb . Form::button('Save', ['class'=>'btn btn-success btn-md', 'type' => 'submit', 'style' => 'width: 100%']),
            'type'    => "btn-button"
        ]
    ];
@endphp

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false])
    @include ('admin.templates.error')

    <div class="row">
        {{ Form::open([
                        'method'         => 'POST', 
                        'url'            => route("$controllerName/save"),
                        'accept-charset' => 'UTF-8',
                        'enctype'        => 'multipart/form-data',
                        'class'          => 'form-horizontal form-label-left',
                        'id'             => 'main-form' ])  }}
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Form'])
                <div class="x_content">
                    {!! FormTemplate::show($elements, ['class' => 'col-md-9 col-sm-9 col-xs-12'])  !!}
                </div>
            </div>
            @include('admin.templates.form_seo', $item)
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Chuyên mục'])
                <div class="x_content">
                    <div class="row">
                        {!! FormTemplate::show($elements_cat, ['class' => 'col-md-12 col-sm-12 col-xs-12'])  !!}
                    </div>
                </div>
            </div>
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Trạng thái'])
                <div class="x_content">
                    <div class="row">
                        {!! FormTemplate::show($elements_status, ['class' => 'col-md-12 col-sm-12 col-xs-12'])  !!}
                    </div>
                </div>
            </div>
            {!! FormTemplate::show($elements_submit)  !!}
        </div>
        {{ Form::close() }}
    </div>
@endsection
