@extends('admin.main')
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;
    use App\Models\MenuModel as Menu;
    use App\Models\CategoryModel ;
    use App\Models\CategoryNewsModel as Cat_News;
    use App\Models\CategoryProductModel as Cat_Product;
    use App\Models\ArticleModel;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label'); 

    
    $targetArrays    = config('zvn.template.target');
    $listTarget      = Template::showListRadio('target', $targetArrays, isset($item['target']) ? $item['target'] : '_self');
    
    $statusValue     = ['active' => ['name' => config('zvn.template.status.active.name')], 'inactive' => ['name' => config('zvn.template.status.inactive.name')]];
    $listStatus      = Template::showListRadio('status', $statusValue, isset($item['status']) ? $item['status'] : 'active');
    
    $item['link']    = (!empty($item['link'])) ? json_decode($item['link']) : null; 

    $linkType        =  [
        'link'          => 'Đường dẫn tùy chọn',
        'category'      => 'Danh mục bài viết',
        //'cat_news'      => 'Danh mục bài viết',
        'news'          => 'Bài viết',
        //'cat_product'   => 'Danh mục sản phẩm',
        //'product'       => 'Sản phẩm',
    ];
    
    $groupLinkMenu   = '';
    foreach ($linkType as $key => $name){
        if($key != 'link')
        {
            switch($key){
                case 'cat_news':
                    $listOptions    = Category::createSelectMenus();
                    break;
                case 'category':
                    $categoryModel  = new CategoryModel();
                    $listCategory   = $categoryModel->listItems(null,['task' => 'admin-list-items-select']); 
                    $listOptions   = array();
                    foreach ($listCategory as $itemCategory)
                    {
                        $listOptions[$itemCategory['id']] = $itemCategory['name'];
                    }
                    break;
                case 'news':
                    $articleModel  = new ArticleModel();
                    $listArticle   = $articleModel->listItems(null,['task' => 'admin-list-items-select']); 
                    $listOptions   = array();
                    foreach ($listArticle as $itemArticle)
                    {
                        $listOptions[$itemArticle['id']] = $itemArticle['name'];
                    }
                    break;
                case 'cat_product':
                    $listOptions    = Cat_Product::createSelectMenus();
                    break;
                case 'product':
                    $listOptions    = Cat_Product::createSelectMenus();
                    break;
                default :
                    $listOptions    = null;
                    break;
            }
            if(!empty($listOptions))
            {
                $itemSelect     = Form::select("link[{$key}][value]", $listOptions , (isset($item['link'])) ? $item['link']->value : null , $formInputAttr);
            }
        }else
        {
            $itemSelect     = Form::text("link[{$key}][value]", (isset($item['link'])) ? $item['link']->value : null, $formInputAttr);
        }
        
        $link_type_active = (isset($item['link']->type)) ? $item['link']->type : 'link'; 
        $itemType = Template::showRadio('link[type]', $key, $name, $link_type_active == $key ? true : false);
        

        $groupLinkMenu  .= '<div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">'.$itemType.'</div>
                                    <div class="col-md-8">'.$itemSelect.'</div>
                                </div>
                            </div>';
    }    
    

    $inputHiddenID   = Form::hidden('id', (isset($item['id']) ? $item['id'] : null));

    
    
    $listParentMenu  = Menu::createSelectMenus();
    $parent   = $listParentMenu;
    
    $elements = [
        [
            'label'   => Form::label('name', 'Name', $formLabelAttr),
            'element' => Form::text('name', isset($item['name']) ? $item['name'] : null, $formInputAttr )
        ],[
            'label'   => Form::label('parent_id', 'Mục cha', $formLabelAttr),
            'element' => Form::select('parent_id', $parent , isset($item['parent_id']) ? $item['parent_id'] : null, $formInputAttr)
        ],[
            'label'   => Form::label('target', 'Kiểu mở', $formLabelAttr),
            'element' => $listTarget
        ],[
            'label'   => Form::label('status', 'Trạng thái', $formLabelAttr),
            'element' => $listStatus
        ],[
            'label'   => Form::label('link', 'Link menu', $formLabelAttr),
            'element' => $groupLinkMenu
        ],[
            'element' => $inputHiddenID . Form::submit('Save', ['class'=>'btn btn-success']),
            'type'    => "btn-submit"   
        ]
    ];
@endphp

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false])
    @include ('admin.templates.error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Form'])
                <div class="x_content">
                    {{ Form::open([
                        'method'         => 'POST', 
                        'url'            => route("$controllerName/save"),
                        'accept-charset' => 'UTF-8',
                        'enctype'        => 'multipart/form-data',
                        'class'          => 'form-horizontal form-label-left',
                        'id'             => 'main-form' ])  }}
                        {!! FormTemplate::show($elements)  !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
