@extends('admin.main')
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');
    $formCkeditor  = config('zvn.template.form_ckeditor');
    $statusValue      = ['default' => 'Select status', 'active' => config('zvn.template.status.active.name'), 'inactive' => config('zvn.template.status.inactive.name')];

    $inputHiddenID    = Form::hidden('id', $item['id']);
    $inputHiddenThumb = Form::hidden('thumb_current', $item['thumb']);
    $network = (!empty($item['network'])) ? @unserialize($item['network']) : array();
    $linkin = (isset($network['linkin'])) ? $network['linkin'] : '';
    $facebook = (isset($network['facebook'])) ? $network['facebook'] : '';
    $twitter = (isset($network['twitter'])) ? $network['twitter'] : '';
    $google = (isset($network['google'])) ? $network['google'] : '';
  
    $elements = [
        [
            'label'   => Form::label('name', 'Tên', $formLabelAttr),
            'element' => Form::text('name', $item['name'],  $formInputAttr )
        ],[
            'label'   => Form::label('position', 'Chức vụ', $formLabelAttr),
            'element' => Form::text('position', $item['position'],  $formInputAttr )
        ],[
            'label'   => Form::label('linkin', 'Linkin', $formLabelAttr),
            'element' => Form::text('network[linkin]', $linkin,  $formInputAttr )
        ],[
            'label'   => Form::label('facebook', 'Facebook', $formLabelAttr),
            'element' => Form::text('network[facebook]', $facebook,  $formInputAttr )
        ],[
            'label'   => Form::label('twitter', 'Twitter', $formLabelAttr),
            'element' => Form::text('network[twitter]', $twitter,  $formInputAttr )
        ],[
            'label'   => Form::label('google', 'Google', $formLabelAttr),
            'element' => Form::text('network[google]', $google,  $formInputAttr )
        ],[
            'label'   => Form::label('status', 'Trạng thái', $formLabelAttr),
            'element' => Form::select('status', $statusValue, $item['status'],  $formInputAttr)
        ],[
            'label'   => Form::label('content', 'Mô tả', $formLabelAttr),
            'element' => Form::textArea('content', $item['content'],  $formCkeditor )
        ],[
            'label'   => Form::label('thumb', 'Thumb', $formLabelAttr),
            'element' => FormTemplate::input_image('thumb', (!empty($item['id'])) ? $item['thumb'] : null, $formInputAttr),
            'thumb'   => (!empty($item['id'])) ? Template::showItemThumb($controllerName, $item['thumb'], $item['name']) : null ,
            'type'    => "thumb" 
        ],[
            'element' => $inputHiddenID . $inputHiddenThumb . Form::submit('Save', ['class'=>'btn btn-success']),
            'type'    => "btn-submit"
        ]
    ];

    $title_page = (isset($item) && !empty($item)) ? 'Cập nhật thông tin bác sĩ' : 'Thêm mới bác sĩ';

@endphp

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false, 'title_page' => $title_page])
    @include ('admin.templates.error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Form'])
                <div class="x_content">
                    {{ Form::open([
                        'method'         => 'POST', 
                        'url'            => route("$controllerName/save"),
                        'accept-charset' => 'UTF-8',
                        'enctype'        => 'multipart/form-data',
                        'class'          => 'form-horizontal form-label-left',
                        'id'             => 'main-form' ])  }}
                        {!! FormTemplate::show($elements)  !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
