@extends('admin.main')
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');
    $formCkeditor  = config('zvn.template.form_ckeditor');
    $statusValue      = ['default' => 'Select status', 'active' => config('zvn.template.status.active.name'), 'inactive' => config('zvn.template.status.inactive.name')];

    $inputHiddenID    = Form::hidden('id', $item['id']);
    $inputHiddenThumb = Form::hidden('image_current', $item['image']);

    $listDistrict  =  array_combine(config('zvn.template.district'), config('zvn.template.district'));
  
    $elements = [
        [
            'label'   => Form::label('name', 'Tên', $formLabelAttr),
            'element' => Form::text('name', $item['name'],  $formInputAttr )
        ],[
            'label'   => Form::label('hotline', 'Hotline', $formLabelAttr),
            'element' => Form::text('hotline', $item['hotline'],  $formInputAttr )
        ],[
            'label'   => Form::label('address', 'Địa chỉ', $formLabelAttr),
            'element' => Form::text('address', $item['address'] , $formInputAttr )
        ],[
            'label'   => Form::label('map', 'Google Map', $formLabelAttr),
            'element' => Form::text('map', $item['map'],  $formInputAttr )
        ],[
            'label'   => Form::label('district', 'Quận', $formLabelAttr),
            'element' => Form::select('district', $listDistrict, $item['district'],  $formInputAttr)
        ],[
            'label'   => Form::label('status', 'Trạng thái', $formLabelAttr),
            'element' => Form::select('status', $statusValue, $item['status'],  $formInputAttr)
        ],[
            'label'   => Form::label('description', 'Mô tả', $formLabelAttr),
            'element' => Form::textArea('description', $item['description'],  $formCkeditor )
        ],[
            'label'   => Form::label('image', 'Thumb', $formLabelAttr),
            'element' => FormTemplate::input_image('image', (!empty($item['id'])) ? $item['image'] : null, $formInputAttr),
            'image'   => (!empty($item['id'])) ? Template::showItemThumb($controllerName, $item['image'], $item['name']) : null ,
            'type'    => "image"
        ],[
            'element' => $inputHiddenID . $inputHiddenThumb . Form::submit('Save', ['class'=>'btn btn-success']),
            'type'    => "btn-submit"
        ]
    ];

    $title_page = (isset($item) && !empty($item)) ? 'Cập nhật thông tin chi nhánh' : 'Thêm mới chi nhánh';

@endphp

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false, 'title_page' => $title_page])
    @include ('admin.templates.error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Form'])
                <div class="x_content">
                    {{ Form::open([
                        'method'         => 'POST', 
                        'url'            => route("$controllerName/save"),
                        'accept-charset' => 'UTF-8',
                        'enctype'        => 'multipart/form-data',
                        'class'          => 'form-horizontal form-label-left',
                        'id'             => 'main-form' ])  }}
                        {!! FormTemplate::show($elements)  !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
