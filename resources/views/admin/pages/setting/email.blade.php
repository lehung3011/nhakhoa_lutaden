@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;
    use App\Models\TagsModel as Tags;

    $tags = Tags::existingTags()->pluck('name');
@endphp


@push('extra_js')
<script src="{{asset('admin/js/selectize/js/standalone/selectize.js')}}"></script>
<script type="text/javascript">
	var tags = [
        @foreach ($tags as $tag)
        {tag: "{{$tag}}" },
        @endforeach
    ];
</script>
@endpush
@push('extra_css')
<link href="{{ asset('admin/js/selectize/css/selectize.bootstrap3.css') }}" rel="stylesheet">
@endpush

@php
    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');

    $formtextAreaAttr = [
        'class' =>  'form-control col-md-6 col-xs-12',
        'rows'  => 4
    ];
    $formtextTags = [
        'class' =>  'form-control col-md-6 col-xs-12 tags',
    ];

    $inputHiddenThumb = Form::hidden('logo_current', (isset($setting['logo'])) ? $setting['logo'] : '');
    $inputHiddenKey = Form::hidden('key', $key);

    $elements_1 = [
        [
            'label'   => Form::label('smtp_username', 'Username', $formLabelAttr),
            'element' => Form::text('setting[smtp_username]', isset($setting['smtp_username']) ? $setting['smtp_username'] : '', $formInputAttr),
        ],[
            'label'   => Form::label('smtp_password', 'Password', $formLabelAttr),
            'element' => Form::password('setting[smtp_password]',  $formtextAreaAttr),
        ],[
            'label'   => Form::label('smtp_name', 'From name', $formLabelAttr),
            'element' => Form::text('setting[smtp_name]', isset($setting['smtp_name']) ? $setting['smtp_name'] : '', $formInputAttr),
        ]
    ];
    
    $elements_2 = [
        [
            'label'   => Form::label('bcc_guest_contact', 'BCC email nhận thông tin Liên hệ', $formLabelAttr),
            'element' => Form::text('setting[bcc_guest_contact]', isset($setting['bcc_guest_contact']) ? $setting['bcc_guest_contact'] : '', $formtextTags),
        ],[
            'label'   => Form::label('bcc_guest_recall', 'BCC email nhận mail gọi lại', $formLabelAttr),
            'element' => Form::text('setting[bcc_guest_recall]', isset($setting['bcc_guest_recall']) ? $setting['bcc_guest_recall'] : '', $formtextTags),
        ],[
            'label'   => Form::label('bcc_guest_order', 'BCC email nhận thông tin đặt lịch hẹn', $formLabelAttr),
            'element' => Form::text('setting[bcc_guest_order]', isset($setting['bcc_guest_order']) ? $setting['bcc_guest_order'] : '', $formtextTags),
        ]
    ];
    
    $elements_submit = [
        [
            'element' => $inputHiddenThumb . $inputHiddenKey . Form::submit('Cập nhật', ['class'=>'btn btn-success']),
            'type'    => "btn-submit-edit"
        ]
    ];

    $title_page = 'Cấu hình Mail';

@endphp
@extends('admin.main')

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false, 'title_page' => $title_page, 'back' => false])
    @include ('admin.templates.error')
    @include ('admin.templates.zvn_notify')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            {{ Form::open([
                'method'         => 'POST', 
                'url'            => route("$controllerName/save",[ 'key' => $key]),
                'accept-charset' => 'UTF-8',
                'enctype'        => 'multipart/form-data',
                'class'          => 'form-horizontal form-label-left',
                'id'             => 'change-password-form',
                'name'           => 'change-password-form' ])  }}
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Send Mail'])
                <div class="x_content">
                    {!! FormTemplate::show($elements_1)  !!}
                </div>
            </div>
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'CC - BCC'])
                <div class="x_content">
                    {!! FormTemplate::show($elements_2)  !!}
                </div>
            </div>
            {!! FormTemplate::show($elements_submit)  !!}
            {{ Form::close() }}
        </div>
    </div>
@endsection
