<!-- menu profile quick info -->
 @if (session('userInfo'))
    @php
        $avata = (session('userInfo')['avatar']) ? asset("images/user/".session('userInfo')['avatar']) : asset('admin/img/user.png');
    @endphp
    <div class="profile clearfix">
        <div class="profile_pic">
            <img src="{{ $avata }}" alt="" class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Xin chào,</span>
            <h2>{{ session('userInfo')['fullname'] }}</h2>
        </div>
    </div>
@endif
<!-- /menu profile quick info -->
<br/>
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Menu</h3>
        <ul class="nav side-menu">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Tổng Quan</a></li>
            <li>
                <a href="javascript:;"><i class="fa fa-user"></i> Người dùng <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('user') }}">Danh sách người dùng</a></li>
                    <li><a href="{{ route('user/change-pass') }}">Đổi mật khẩu</a></li>
                </ul>
            </li>
            <li><a href="{{ route('category') }}"><i class="fa fa fa-building-o"></i> Chuyên mục</a></li>
            <li><a href="{{ route('article') }}"><i class="fa fa-newspaper-o"></i> Bài viết</a></li>
            <li><a href="{{ route('slider') }}"><i class="fa fa-sliders"></i> Silders</a></li>
            <li><a href="{{ route('dentists') }}"><i class="fa fa-user-md"></i> Bác sĩ</a></li>
            <li><a href="{{ route('comments') }}"><i class="fa fa-comments"></i> Nhận xét</a></li>
            <li><a href="{{ route('agencies/index') }}"><i class="fa fa-university"></i> Chi nhánh</a></li>
            <li><a href="{{ route('faqs/index') }}"><i class="fa fa-question"></i> FAQ</a></li>
            <li><a href="{{ route('comments') }}"><i class="fa fa-phone"></i> Lịch hẹn</a></li>
            <li><a href="{{ route('comments') }}"><i class="fa fa-list-alt"></i> Yêu cầu gọi lại</a></li>
            <li><a href="{{ route('menu') }}"><i class="fa fa-list-ul"></i> Quản lý menu</a></li>
            <li><a href="{{ route('media') }}"><i class="fa fa-file-image-o"></i> Thư viện ảnh</a></li>
            <li>
                <a href="javascript:;"><i class="fa fa-cog"></i> Cấu hình <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('setting/general') }}">Cấu hình chung</a></li>
                    <li><a href="{{ route('setting/email') }}">Mail - BCC</a></li>
                    <li><a href="{{ route('setting/social') }}">Mạng xã hội</a></li>
                    <li><a href="{{ route('setting/chat') }}">Chat Facebook</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /sidebar menu -->
