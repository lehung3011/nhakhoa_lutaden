@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');
    $formTextareaAttr = config('zvn.template.form_textarea');
    
    $elements = [
        [
            'label'   => Form::label('metatitle', 'Tiêu đề trang', $formLabelAttr),
            'element' => Form::text('metatitle', $item['metatitle'],  $formInputAttr )
        ],[
            'label'   => Form::label('metadesc', 'Meta description', $formLabelAttr),
            'element' => Form::textArea('metadesc', $item['metadesc'],  $formTextareaAttr)
        ],[
            'label'   => Form::label('metakey', 'Meta keywords', $formLabelAttr),
            'element' => Form::textArea('metakey', $item['metakey'],  $formTextareaAttr )
        ],
    ];
@endphp
<div class="x_panel">
    @include('admin.templates.x_title', ['title' => 'SEO'])
    <div class="x_content">
         {!! FormTemplate::show($elements, ['class' => 'col-md-9 col-sm-9 col-xs-12'])  !!}
    </div>
</div>