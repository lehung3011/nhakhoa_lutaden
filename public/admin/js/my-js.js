$(document).ready(function () {
    let $btnSearch = $("button#btn-search");
    let $btnClearSearch = $("button#btn-clear-search");

    let $inputSearchField = $("input[name  = search_field]");
    let $inputSearchValue = $("input[name  = search_value]");
    let $selectChangeAttr = $("select[name = select_change_attr]");
    let $selectChangeAttrAjax = $("select[name =  select_change_attr_ajax]");

    /*===================================================================*/
    $("a.select-field").click(function (e) {
        e.preventDefault();

        let field = $(this).data("field");
        let fieldName = $(this).html();
        $("button.btn-active-field").html(
            fieldName + ' <span class="caret"></span>'
        );
        $inputSearchField.val(field);
    });

    /*===================================================================*/
    $btnSearch.click(function () {
        var pathname = window.location.pathname;
        let params = ["filter_status"];
        let searchParams = new URLSearchParams(window.location.search); // ?filter_status=active

        let link = "";
        $.each(params, function (key, param) {
            // filter_status
            if (searchParams.has(param)) {
                link += param + "=" + searchParams.get(param) + "&"; // filter_status=active
            }
        });

        let search_field = $inputSearchField.val();
        let search_value = $inputSearchValue.val();

        if (search_value.replace(/\s/g, "") == "") {
            alert("Nhập vào giá trị cần tìm !!");
        } else {
            window.location.href =
                pathname +
                "?" +
                link +
                "search_field=" +
                search_field +
                "&search_value=" +
                search_value;
        }
    });

    /*===================================================================*/
    $btnClearSearch.click(function () {
        var pathname = window.location.pathname;
        let searchParams = new URLSearchParams(window.location.search);

        params = ["filter_status"];

        let link = "";
        $.each(params, function (key, param) {
            if (searchParams.has(param)) {
                link += param + "=" + searchParams.get(param) + "&";
            }
        });

        window.location.href = pathname + "?" + link.slice(0, -1);
    });

    /*===================================================================*/
    $(".btn-delete").on("click", function () {
        if (!confirm("Bạn có chắc muốn xóa phần tử?")) return false;
    });

    /*===================================================================*/
    $selectChangeAttr.on("change", function () {
        let selectValue = $(this).val();
        let url = $(this).data("url");
        console.log(url.replace("value_new", selectValue));
        window.location.href = url.replace("value_new", selectValue);
    });

    /*===================================================================*/
    $selectChangeAttrAjax.on("change", function () {
        let select_value = $(this).val();
        let $url = $(this).data("url");
        let csrf_token = $("input[name=csrf-token]").val();

        $.ajax({
            url: $url.replace("value_new", select_value),
            type: "GET",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": csrf_token,
            },
            success: function (result) {
                if (result) {
                    $.notify("Cập nhật giá trị thành công!", {
                        position: "top center",
                        className: "success",
                    });
                } else {
                    console.log(result);
                }
            },
        });
    });

    /*===================================================================*/
    var editor_config = {
        path_absolute: "/",
        selector: "textarea.tinyeditor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern",
        ],
        toolbar:
            "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback: function (field_name, url, type, win) {
            var x =
                window.innerWidth ||
                document.documentElement.clientWidth ||
                document.getElementsByTagName("body")[0].clientWidth;
            var y =
                window.innerHeight ||
                document.documentElement.clientHeight ||
                document.getElementsByTagName("body")[0].clientHeight;

            var cmsURL =
                editor_config.path_absolute +
                "laravel-filemanager?field_name=" +
                field_name;
            if (type == "image") {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: "Filemanager",
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no",
            });
        },
    };

    tinymce.init(editor_config);

    /*===================================================================*/
    var route_prefix = "/laravel-filemanager";
    if ($("body").find("#lfm").length > 0) {
        $("#lfm").filemanager("image", { prefix: route_prefix });
    }
    /*===================================================================*/
    $(".tags").selectize({
        delimiter: ",",
        persist: false,
        valueField: "tag",
        labelField: "tag",
        searchField: "tag",
        options: tags,
        create: function (input) {
            return {
                tag: input,
            };
        },
    });
});
