/***************************************************************************************************************
||||||||||||||||||||||||||||         CUSTOM SCRIPT FOR Medical clinic            ||||||||||||||||||||||||||||||||||||
****************************************************************************************************************
||||||||||||||||||||||||||||              TABLE OF CONTENT                  ||||||||||||||||||||||||||||||||||||
****************************************************************************************************************
****************************************************************************************************************
1 revolutionSliderActiver
2 galleryMasonaryLayout
3 accrodion
4 teamCarosule
5 testiCarosule
6 CounterNumberChanger
7 stickyHeader
8 contactFormValidation
9 selectInput
10 datePicker
11 gMap
12 mobileMenu
****************************************************************************************************************
||||||||||||||||||||||||||||            End TABLE OF CONTENT                ||||||||||||||||||||||||||||||||||||
****************************************************************************************************************/

"use strict";

// 1 revolutionSliderActiver
function initHomeSlider() {
    if ($(".home_slider").length) {
        var homeSlider = $(".home_slider");
        homeSlider.owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 8000,
            dots: false,
            nav: false,
            smartSpeed: 1200,
        });

        // Prev Navigation
        if ($(".home_slider_prev").length) {
            var prev = $(".home_slider_prev");
            prev.on("click", function () {
                homeSlider.trigger("prev.owl.carousel");
            });
        }

        // Next Navigation
        if ($(".home_slider_next").length) {
            var next = $(".home_slider_next");
            next.on("click", function () {
                homeSlider.trigger("next.owl.carousel");
            });
        }

        // add animate.css class(es) to the elements to be animated
        function setAnimation(_elem, _InOut) {
            // Store all animationend event name in a string.
            // cf animate.css documentation
            var animationEndEvent =
                "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

            _elem.each(function () {
                var $elem = $(this);
                var $animationType =
                    "animated " + $elem.data("animation-" + _InOut);

                $elem
                    .addClass($animationType)
                    .one(animationEndEvent, function () {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
            });
        }

        // Fired before current slide change
        homeSlider.on("change.owl.carousel", function (event) {
            var $currentItem = $(".home_slider_item", homeSlider).eq(
                event.item.index
            );
            var $elemsToanim = $currentItem.find("[data-animation-out]");
            setAnimation($elemsToanim, "out");
        });

        // Fired after current slide has been changed
        homeSlider.on("changed.owl.carousel", function (event) {
            var $currentItem = $(".home_slider_item", homeSlider).eq(
                event.item.index
            );
            var $elemsToanim = $currentItem.find("[data-animation-in]");
            setAnimation($elemsToanim, "in");
        });
    }
}

function revolutionSliderActiver() {
    if ($(".rev_slider_wrapper #slider1").length) {
        $("#slider1").revolution({
            sliderType: "standard",
            sliderLayout: "auto",
            delay: 100000,
            navigation: {
                arrows: { enable: true },
            },
            responsiveLevels: [1240, 1024, 778, 480],
            gridwidth: 1170,
            gridheight: 770,
            visibilityLevels: [1240, 1024, 1024, 480],
        });
    }
}
// 2 galleryMasonaryLayout
function galleryMasonaryLayout() {
    if ($(".img-masonary").length) {
        $(".img-masonary").isotope({
            layoutMode: "masonry",
        });
    }
}
// 3 accrodion
function accrodion() {
    if ($(".accrodion-grp").length) {
        $(".accrodion-grp").each(function () {
            var accrodionName = $(this).data("grp-name");
            var Self = $(this);
            console.log(Self);
            Self.addClass(accrodionName);
            Self.find(".accrodion .accrodion-content").hide();
            Self.find(".accrodion.active").find(".accrodion-content").show();
            Self.find(".accrodion").each(function () {
                console.log($(this));
                $(this)
                    .find(".accrodion-title")
                    .on("click", function () {
                        if ($(this).parent().hasClass("active") === false) {
                            $(".accrodion-grp." + accrodionName)
                                .find(".accrodion")
                                .removeClass("active");
                            $(".accrodion-grp." + accrodionName)
                                .find(".accrodion")
                                .find(".accrodion-content")
                                .slideUp();
                            $(this).parent().addClass("active");
                            $(this)
                                .parent()
                                .find(".accrodion-content")
                                .slideDown();
                        }
                    });
            });
        });
    }
}

// 4 teamCarosule
function teamCarosule() {
    if ($(".team-carousel").length) {
        $(".team-carousel").owlCarousel({
            loop: true,
            margin: 30,
            nav: true,
            dots: false,
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>',
            ],
            autoplay: 5000,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                },
                480: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                },
                1200: {
                    items: 4,
                },
            },
        });
    }
}
// 5 testiCarosule
function testiCarosule() {
    if ($(".testimonaials-carousel").length) {
        $(".testimonaials-carousel").owlCarousel({
            loop: true,
            margin: 50,
            nav: false,
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>',
            ],
            dots: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                },
                480: {
                    items: 1,
                },
                768: {
                    items: 1,
                    nav: true,
                    dots: false,
                },
                1000: {
                    items: 2,
                },
                1200: {
                    items: 3,
                },
            },
        });
    }
}
// 5 testiCarosule
function testiCarosule2() {
    if ($(".three-item-carousel").length) {
        $(".three-item-carousel").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            autoplay: true,
            fade: false,
            focusOnSelect: true,
            autoplaySpeed: 5000,
            speed: 800,
            dots: true,
        });
    }
}
// 6 CounterNumberChanger
function CounterNumberChanger() {
    var timer = $(".timer");
    if (timer.length) {
        timer.appear(function () {
            timer.countTo();
        });
    }
}
// 7 stickyHeader
function stickyHeader() {
    if ($(".stricky").length) {
        var strickyScrollPos = $(".stricky").next().offset().top;
        if ($(window).scrollTop() > strickyScrollPos) {
            $(".stricky").removeClass("fadeIn animated");
            $(".stricky").addClass("stricky-fixed fadeInDown animated");
        } else if ($(this).scrollTop() <= strickyScrollPos) {
            $(".stricky").removeClass("stricky-fixed fadeInDown animated");
            $(".stricky").addClass("slideIn animated");
        }
    }
}

//Progress Bar / Levels
if ($(".progress-levels .progress-box .bar-fill").length) {
    $(".progress-box .bar-fill").each(function () {
        var progressWidth = $(this).attr("data-percent");
        $(this).css("width", progressWidth + "%");
        //$(this).parents('.progress-box').children('.percent').html(progressWidth+'%');
    });
}

// 8 contactFormValidation
function contactFormValidation() {
    if ($(".contact-form").length) {
        $(".contact-form").each(function () {
            var cfName = $(this).attr("id");

            $("#" + cfName).validate({
                // initialize the plugin
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    message: {
                        required: true,
                    },
                    date: {
                        required: true,
                    },
                    category: {
                        required: true,
                    },
                    phone: {
                        required: true,
                    },
                    gender: {
                        required: true,
                    },
                    dateOfBirth: {
                        required: true,
                    },
                    subject: {
                        required: true,
                    },
                },
                submitHandler: function (form) {
                    // sending value with ajax request
                    $.post(
                        $(form).attr("action"),
                        $(form).serialize(),
                        function (response) {
                            $(form).parent("div").append(response);
                            $(form).find('input[type="text"]').val("");
                            $(form).find('input[type="email"]').val("");
                            $(form).find("textarea").val("");
                        }
                    );
                    return false;
                },
            });
        });
    }
}

// 9 selectInput
function selectInput() {
    if ($(".select-input").length) {
        $(".select-input").selectmenu();
    }
}
// 10 datePicker
function datePicker() {
    // if ($('.date-picker').length) {
    // 	$('.date-picker').datepicker({format: "d/m/Y" });
    // };

    $("#datetimepicker").datetimepicker({ format: "H:i d/m/Y" });
}

// 11 gMap
function gMap() {
    if ($(".google-map").length) {
        $(".google-map").each(function () {
            // getting options from html
            var mapName = $(this).attr("id");
            var mapLat = $(this).data("map-lat");
            var mapLng = $(this).data("map-lng");
            var iconPath = $(this).data("icon-path");
            var mapZoom = $(this).data("map-zoom");
            var mapTitle = $(this).data("map-title");

            // if zoom not defined the zoom value will be 15;
            if (!mapZoom) {
                var mapZoom = 15;
            }
            // init map
            var map;
            map = new GMaps({
                div: "#" + mapName,
                scrollwheel: false,
                lat: mapLat,
                lng: mapLng,
                zoom: mapZoom,
            });
            // if icon path setted then show marker
            if (iconPath) {
                map.addMarker({
                    icon: iconPath,
                    lat: mapLat,
                    lng: mapLng,
                    title: mapTitle,
                });
            }
        });
    }
}
// 12 mobileMenu
function mobileMenu() {
    if ($(".navigation .nav-footer button").length) {
        $(".navigation .nav-footer button").on("click", function () {
            $(".navigation .nav-header").slideToggle();
            $(".navigation .nav-header")
                .find(".dropdown")
                .children("a")
                .append(function () {
                    return '<button><i class="fa fa-bars"></i></button>';
                });
            $(".navigation .nav-header .dropdown a button").on(
                "click",
                function () {
                    $(this)
                        .parent()
                        .parent()
                        .children("ul.submenu")
                        .slideToggle();
                    return false;
                }
            );
        });
    }
}
//13 scroll top
function scrollTop() {
    //scroll-top

    if ($("#back-to-top").length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $("#back-to-top").addClass("show");
                } else {
                    $("#back-to-top").removeClass("show");
                }
            },
            fixedSearch = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > 0) {
                    $(".main-header").addClass("fixed-search");
                } else {
                    $(".main-header").removeClass("fixed-search");
                }
            };

        backToTop();
        $(window).on("scroll", function () {
            backToTop();
            fixedSearch();
        });
        $("#back-to-top").on("click", function (e) {
            e.preventDefault();
            $("html,body").animate(
                {
                    scrollTop: 0,
                },
                700
            );
        });
    }
}

// Elements Animation
if ($(".wow").length) {
    var wow = new WOW({
        boxClass: "wow", // animated element css class (default is wow)
        animateClass: "animated", // animation css class (default is animated)
        offset: 0, // distance to the element when triggering the animation (default is 0)
        mobile: true, // trigger animations on mobile devices (default is true)
        live: true, // act on asynchronously loaded content (default is true)
    });
    wow.init();
}

// Dom Ready Function
jQuery(document).on("ready", function () {
    (function ($) {
        // add your functions
        // revolutionSliderActiver();
        initHomeSlider();
        accrodion();
        galleryMasonaryLayout();
        // teamCarosule();
        CounterNumberChanger();
        // testiCarosule();
        testiCarosule2();
        // contactFormValidation();
        selectInput();
        datePicker();
        // gMap();
        mobileMenu();
        scrollTop();
    })(jQuery);
});
// window on load functino
jQuery(window).on("load", function () {
    (function ($) {
        // add your functions
    })(jQuery);
});
// window on scroll functino
jQuery(window).on("scroll", function () {
    (function ($) {
        // add your functions
        stickyHeader();
    })(jQuery);
});
