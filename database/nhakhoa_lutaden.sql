-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 05, 2020 at 11:22 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nhakhoa_lutaden`
--

-- --------------------------------------------------------

--
-- Table structure for table `agencies`
--

DROP TABLE IF EXISTS `agencies`;
CREATE TABLE IF NOT EXISTS `agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) NOT NULL DEFAULT '0',
  `name` varchar(200) NOT NULL DEFAULT '0',
  `address` varchar(300) NOT NULL DEFAULT '0',
  `description` varchar(1000) NOT NULL DEFAULT '0',
  `hotline` varchar(200) NOT NULL DEFAULT '0',
  `map` varchar(500) DEFAULT NULL,
  `district` varchar(100) NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `agencies`
--

INSERT INTO `agencies` (`id`, `image`, `name`, `address`, `description`, `hotline`, `map`, `district`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, '/uploads/agencies/4znnEqmWuM.png', 'Chi nhánh 1', '7A/19/15 Thành Thái, Phường 14, Quận 10, Hồ Chí Minh', '<p><strong>GIỜ L&Agrave;M VIỆC</strong></p>\r\n<p>08:00 &ndash; 20:00&nbsp; (Thứ 2 &ndash; Thứ 7)</p>\r\n<p>08:00 &ndash; 17:00&nbsp; (Chủ Nhật)</p>\r\n<p>Trưa l&agrave;m b&igrave;nh thường, giữ xe miễn ph&iacute;</p>', '02822670999,02822672666', 'https://www.google.com/maps/place/557%2FF11+bis+Nguy%E1%BB%85n+Tri+Ph%C6%B0%C6%A1ng+T%E1%BB%95+67+KP+8,+Ph%C6%B0%E1%BB%9Dng+14,+Qu%E1%BA%ADn+10,+H%E1%BB%93+Ch%C3%AD+Minh,+Vi%E1%BB%87t+Nam/@10.7701091,106.6619296,19z/data=!4m16!1m10!4m9!1m3!2m2!1d106.6617874!2d10.7705109!1m3!2m2!1d106.6616587!2d10.7704793!3e0!3m4!1s0x31752ec28971efc1:0xc7261a1f1714138c!8m2!3d10.770439!4d106.6617799', 'Quận 10', 'active', '2019-06-18 02:06:57', 'admin', '2020-06-24 00:00:00', 'admin'),
(2, '/uploads/agencies/zJDBVV5coR.png', 'Chi nhánh 2', '757C Kha Vạn Cân, P.Linh Tây, Thủ Đức, Hcm', '<p><strong>GIỜ L&Agrave;M VIỆC</strong></p>\r\n<p>08:00 &ndash; 20:00&nbsp; (Thứ 2 &ndash; Thứ 7)</p>\r\n<p>08:00 &ndash; 16:00&nbsp; (Chủ Nhật)</p>\r\n<p>Trưa l&agrave;m b&igrave;nh thường, giữ xe miễn ph&iacute;,&nbsp;c&oacute; chỗ đậu &Ocirc; t&ocirc;</p>', '02822670999,02822460777', 'https://www.google.com/maps/dir/10.8595256,106.8060391/715+Kha+V%E1%BA%A1n+C%C3%A2n,+Th%E1%BB%A7+%C4%90%E1%BB%A9c,+H%E1%BB%93+Ch%C3%AD+Minh,+Vi%E1%BB%87t+Nam/@10.854276,106.7667421,14z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x31752796275babe3:0x8d2aa1826f015fba!2m2!1d106.7548448!2d10.851561', 'Quận Thủ Đức', 'active', '2019-06-18 02:06:08', 'admin', '2020-06-24 00:00:00', 'admin'),
(3, '/uploads/agencies/1cRg5Y2cpT.png', 'Chi nhánh 3', '523 Đỗ Xuân Hợp, Block C chung cư The Art, KDCQ10', '<p><strong>GIỜ L&Agrave;M VIỆC</strong></p>\r\n<p>08:00 &ndash; 20:00&nbsp; (Thứ 2 &ndash; Thứ 7)</p>\r\n<p>08:00 &ndash; 16:00&nbsp; (Chủ Nhật)</p>\r\n<p>Trưa l&agrave;m b&igrave;nh thường, giữ xe miễn ph&iacute;,&nbsp;c&oacute; chỗ đậu &Ocirc; t&ocirc;</p>', '02822460777,02822670999', 'https://www.google.com/maps/dir/10.8595256,106.8060391/The+Art+Gia+Ho%C3%A0/@10.8341531,106.7555064,13z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x317526f1174b233f:0xa971d76ccafd5cb9!2m2!1d106.7752245!2d10.815756', 'Quận 9', 'active', '2019-06-18 02:06:51', 'admin', '2020-06-24 00:00:00', 'admin'),
(4, '/uploads/agencies/XUUsugby2I.jpeg', 'Chi nhánh 4', 'BLOCK B4 CHUNG CƯ HIM LAM CHỢ LỚN - 491 HẬU GIANG , PHƯỜNG 11, QUẬN 6', '<p><strong>GIỜ L&Agrave;M VIỆC</strong></p>\r\n<p>08:00 &ndash; 20:00&nbsp; (Thứ 2 &ndash; Thứ 7)</p>\r\n<p>08:00 &ndash; 17:00&nbsp; (Chủ Nhật)</p>\r\n<p>Trưa l&agrave;m b&igrave;nh thường, giữ xe miễn ph&iacute;</p>', '02822672666', NULL, 'Quận 6', 'active', '2020-06-18 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(5, '/uploads/agencies/KybW7ux1RY.jpeg', 'Viện nha khoa kỹ thuật cao Venice', '410 Cao Thắng, Phường 12, Quận 10', '<p><strong>GIỜ L&Agrave;M VIỆC</strong></p>\r\n<p>08:00 &ndash; 20:00&nbsp; (Thứ 2 &ndash; Thứ 7)</p>\r\n<p>08:00 &ndash; 17:00&nbsp; (Chủ Nhật)</p>\r\n<p>Trưa l&agrave;m b&igrave;nh thường, giữ xe miễn ph&iacute;</p>', '02822480555', NULL, 'Quận 10', 'active', '2019-07-09 21:07:21', 'admin', '2020-06-24 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `category_id`, `name`, `content`, `status`, `thumb`, `type`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 9, 'GIỚI THIỆU CHUNG', '<p>Ra đời từ th&aacute;ng&nbsp;1/2014 với cơ sở duy nhất tại địa chỉ 39 Quang Trung&nbsp; &ndash; TP H&agrave; Nội, sau đó, bằng sự tin tưởng của kh&aacute;ch h&agrave;ng,&nbsp;Nha khoa đ&atilde; mở rộng hệ thống cơ sở tr&ecirc;n khắp cả nước v&agrave; trở th&agrave;nh hệ thống chuỗi nha khoa ti&ecirc;u chu&acirc;̉n Pháp uy t&iacute;n, được h&agrave;ng ng&agrave;n kh&aacute;ch h&agrave;ng gần xa t&iacute;n nhiệm.</p>\r\n<p>Tính đ&ecirc;́n thời đi&ecirc;̉m hi&ecirc;̣n tại, Nha khoa có t&ocirc;̉ng 3&nbsp;cơ sở Tp, H&ocirc;̀ Chí Minh.</p>\r\n<ul>\r\n<li>Chi nh&aacute;nh 1 : 523 Đỗ Xu&acirc;n Hợp, Block C chung cư The Art, KDCQ10</li>\r\n<li>Chi nh&aacute;nh 2 : 7A/19/15 Th&agrave;nh Th&aacute;i, P14, Q10,Hcm</li>\r\n<li>Chi nh&aacute;nh 3 : 757C Kha Vạn C&acirc;n, P.Linh T&acirc;y, Thủ Đức, Hcm</li>\r\n</ul>\r\n<p>Định hướng của hệ thống chuỗi nha khoa l&agrave; ph&aacute;t triển bền vững theo ti&ecirc;u chuẩn: TH&Acirc;̉M MỸ &ndash; AN TOÀN &ndash; HI&Ecirc;̣N ĐẠI. Tương lai kh&ocirc;ng xa, Nha khoa sẽ ti&ecirc;́p tục phát tri&ecirc;̉n và mở r&ocirc;̣ng th&ecirc;m các cơ sở nhằm đem lại cho khách hàng những dịch vụ chăm sóc răng mi&ecirc;̣ng ti&ecirc;u chu&acirc;̉n Pháp ngay tại Vi&ecirc;̣t Nam.</p>', 'active', '/uploads/article/0NzUEKfBOf.jpeg', NULL, '2020-06-16 00:00:00', 'hailan', '2020-06-24 00:00:00', 'admin'),
(2, 2, 'Bảng Giá', '<h3><strong>Bảng gi&aacute; dịch vụ tại Lutadent</strong></h3>\r\n<table style=\"width: 700px;\" border=\"1\" cellspacing=\"1\" cellpadding=\"1\">\r\n<thead>\r\n<tr>\r\n<th scope=\"col\">Kh&aacute;m v&agrave; tư vấn răng</th>\r\n<th scope=\"col\">Gi&aacute; (VNĐ)</th>\r\n<th scope=\"col\">Đơn vị</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>Kh&aacute;m v&agrave; tư vấn</td>\r\n<td>MIỄN PH&Iacute;</td>\r\n<td>Lần</td>\r\n</tr>\r\n<tr>\r\n<td>Chụp X-quang</td>\r\n<td>MIỄN PH&Iacute;</td>\r\n<td>Lần</td>\r\n</tr>\r\n<tr>\r\n<td>Chụp CT</td>\r\n<td>MIỄN PH&Iacute;</td>\r\n<td>Khi Cắm Implant</td>\r\n</tr>\r\n<tr>\r\n<td>Chụp Cephalo</td>\r\n<td>MIỄN PH&Iacute;</td>\r\n<td>Khi niềng răng</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table style=\"width: 700px;\" border=\"1\" cellspacing=\"1\" cellpadding=\"1\">\r\n<thead>\r\n<tr>\r\n<th>Phục h&igrave;nh răng sứ</th>\r\n<th>Gi&aacute; (VNĐ)</th>\r\n<th>Đơn vị</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>Răng sứ Mỹ</td>\r\n<td>1.000.000 - 1.500.000</td>\r\n<td>1 răng</td>\r\n</tr>\r\n<tr>\r\n<td>Răng sứ Titan</td>\r\n<td>2.000.000 - 2.500.000</td>\r\n<td>1 răng</td>\r\n</tr>\r\n<tr>\r\n<td>Răng sứ Zirconia Crystal</td>\r\n<td>2.500.000</td>\r\n<td>1 răng</td>\r\n</tr>\r\n<tr>\r\n<td>Răng sứ Cercon Heat (Cercon HT)</td>\r\n<td>2.000.000&nbsp;</td>\r\n<td>1 răng</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'active', '/uploads/article/pmhM9OVfkp.png', NULL, '2020-06-17 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(3, 1, 'NIỀNG RĂNG TRẢ GÓP CHỈ TỪ 1TR/THÁNG', '<p><strong>ƯU Đ&Atilde;I NIỀNG RĂNG KH&Ocirc;NG L&Atilde;I SUẤT</strong></p>\r\n<p>Trả trước chỉ 30%</p>\r\n<p>Trả g&oacute;p chỉ từ 1 triệu/th&aacute;ng</p>\r\n<p><strong>Đối tượng &aacute;p dụng</strong>: Học sinh/sinh vi&ecirc;n.&nbsp;</p>', 'active', '/uploads/article/NUX2GrYLlp.jpeg', NULL, '2020-06-17 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(4, 1, 'LÀM RĂNG TRẢ GÓP CHỈ TỪ 699K/THÁNG', '<p><strong>L&Agrave;M RĂNG TRẢ G&Oacute;P 0% L&Atilde;I SUẤT</strong></p>\r\n<p>&Aacute;p dụng: Tất cả dịch vụ - Từ 6 răng trở l&ecirc;n</p>\r\n<p>Trả g&oacute;p chỉ từ 669k/th&aacute;ng&nbsp;</p>\r\n<p><strong>➤ Áp dụng: Các cơ sở Nha khoa Lutadent&nbsp;</strong></p>\r\n<p><em><strong>Kh&ocirc;ng &aacute;p dụng đồng thời c&aacute;c chương tr&igrave;nh khuyến m&atilde;i kh&aacute;c</strong></em></p>', 'active', '/uploads/article/4t8ddMbgL8.jpeg', NULL, '2020-06-17 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(5, 1, 'GIẢM 70% COMBO TẨY TRẮNG RĂNG', '<p><strong>Combo Tẩy trắng răng&nbsp;</strong>gi&aacute; gốc&nbsp;<s>3.000.000</s>&nbsp;ưu đ&atilde;i chỉ c&ograve;n<strong>&nbsp;999k&nbsp;</strong></p>\r\n<p><strong>Đặc biệt:</strong></p>\r\n<ul>\r\n<li>Tặng k&egrave;m g&oacute;i Chăm s&oacute;c răng miệng.</li>\r\n<li>Miền ph&iacute; kem duy tr&igrave; độ trắng.&nbsp;</li>\r\n</ul>\r\n<p>***Ưu đ&atilde;i m&aacute;ng ng&acirc;m mang về nh&agrave;&nbsp;<s>&nbsp;1.000.000&nbsp;</s>&nbsp;chỉ c&ograve;n 500k</p>', 'active', '/uploads/article/W6PNikIVK8.jpeg', NULL, '2020-06-17 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(6, 1, 'GIẢM 50% NIỀNG RĂNG TRONG SUỐT', '<p><strong>GIẢM 50% NIỀNG RĂNG TRONG SUỐT</strong></p>\r\n<p>Chỉ c&ograve;n<strong>&nbsp;39.999.000</strong>&nbsp;(gi&aacute; gốc<s>&nbsp;80.000.000</s>)</p>\r\n<p><strong>Tặng&nbsp;k&egrave;m:&nbsp;&nbsp;</strong><em>G&oacute;i Chăm s&oacute;c răng miệng<strong>&nbsp;trị gi&aacute; 10 triệu.&nbsp;</strong></em></p>', 'active', '/uploads/article/2BeGUnvJLg.jpeg', NULL, '2020-06-17 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(7, 5, 'Nguyên nhân làm cho hơi thở không được thơm mát', '<ul>\r\n<li>Nguy&ecirc;n nh&acirc;n đầu ti&ecirc;n đ&oacute; ch&iacute;nh l&agrave; việc vệ sinh răng miệng kh&ocirc;ng đ&uacute;ng c&aacute;ch, kh&ocirc;ng sạch sẽ, tạo cơ hội cho c&aacute;c vi khuẩn xấu trong khoang miệng ph&aacute;t triển g&acirc;y ra m&ugrave;i h&ocirc;i kh&oacute; chịu.</li>\r\n<li>Kh&ocirc;ng chỉ vậy, vệ sinh răng miệng kh&ocirc;ng đ&uacute;ng c&aacute;ch c&ograve;n l&agrave;m cho c&aacute;c bạn bị s&acirc;u răng,&nbsp;<a href=\"https://dep365.com/nhung-thoi-quen-thuc-an-khien-rang-bi-o-vang-can-tranh/\">răng ố v&agrave;ng</a>, nha chu, nhiễm tr&ugrave;ng nướu&hellip;</li>\r\n<li>Bạn ăn những m&oacute;n ăn c&oacute; m&ugrave;i nặng v&agrave; kh&ocirc;ng vệ sinh răng miệng ngay sau đ&oacute;!</li>\r\n</ul>\r\n<p><img src=\"https://nhakhoalutadent.com/files/images/DSC_1444.jpg\" alt=\"\" /></p>', 'active', '/uploads/article/djiaZgaIYa.png', NULL, '2020-06-17 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(8, 5, 'Cách chăm sóc sức khỏe răng miệng cho người già', '<p>D&ugrave; l&agrave; c&ograve;n răng hay mất răng, người cao tuổi cũng n&ecirc;n đi kh&aacute;m răng định kỳ 3-6 th&aacute;ng/ lần, để ph&aacute;t hiện v&agrave; điều trị sớm c&aacute;c bệnh về răng nếu c&oacute;.</p>\r\n<p><strong><em>1. Dinh dưỡng hợp l&yacute;</em></strong></p>\r\n<p>C&aacute;c loại rau quả tươi l&agrave; nguồn cung cấp vitamin dồi d&agrave;o cho cơ thể n&oacute;i chung, cho răng v&agrave; lợi n&oacute;i ri&ecirc;ng; ch&uacute;ng cũng c&oacute; t&aacute;c dụng l&agrave;m sạch răng sau khi ăn.</p>\r\n<p>N&ecirc;n ăn tr&aacute;i c&acirc;y tươi thay cho c&aacute;c loại b&aacute;nh ngọt. Chỉ n&ecirc;n ăn b&aacute;nh ngọt v&agrave;o bữa ch&iacute;nh v&agrave; đ&aacute;nh răng ngay sau đ&oacute;. Thời điểm ăn tr&aacute;i c&acirc;y tươi tốt nhất l&agrave; trước bữa ăn ch&iacute;nh 1 giờ đồng hồ, v&igrave; ch&uacute;ng l&agrave; đồ ăn sống. Ăn đồ sống trước khi ăn đồ ch&iacute;n sẽ gi&uacute;p tr&aacute;nh c&aacute;c phản ứng tăng bạch cầu, bảo vệ hệ thống miễn dịch của cơ thể.</p>\r\n<p>Người cao tuổi thường ăn &iacute;t v&agrave; chia l&agrave;m nhiều bữa nhỏ. V&igrave; vậy, sau mỗi lần ăn, phải s&uacute;c miệng v&agrave; chải răng ngay, kh&ocirc;ng để thức ăn lưu lại tr&ecirc;n răng v&agrave; lợi, tạo điều kiện cho vi khuẩn sinh s&ocirc;i dẫn đến s&acirc;u răng.</p>\r\n<p>Chế độ ăn n&ecirc;n ăn đủ c&aacute;c chất như: Đạm (c&oacute; trong: thịt, trứng, t&ocirc;m, cua, sữa, đậu phụ); c&aacute;c vitamin (tr&aacute;i c&acirc;y); muối kho&aacute;ng; chất b&eacute;o thực vật, hạn chế tối đa ăn mỡ v&agrave; phủ tạng động vật.</p>\r\n<p><strong><em>2. Ph&ograve;ng bệnh nha chu</em></strong></p>\r\n<p>Mảng b&aacute;m của vi khuẩn l&agrave; nguy&ecirc;n nh&acirc;n g&acirc;y bệnh nha chu. Nếu kh&ocirc;ng chải răng kỹ v&agrave; d&ugrave;ng chỉ nha khoa, mảng b&aacute;m sẽ d&agrave;y dần v&agrave; g&acirc;y vi&ecirc;m nướu.</p>\r\n<p>Triệu chứng ban đầu của bệnh nha chu l&agrave; c&oacute; v&ocirc;i b&aacute;m ở cổ răng, k&iacute;ch th&iacute;ch g&acirc;y ra vi&ecirc;m nướu. C&aacute;c m&ocirc; quanh ch&acirc;n răng (như lợi, xương, men gốc răng, d&acirc;y chằng) đ&atilde; bị ph&aacute; hủy. Răng mất điểm tựa vững chắc n&ecirc;n từ từ lung lay v&agrave; thưa dần; l&uacute;c n&agrave;y răng biểu hiện sẽ l&agrave; sưng lợi, lợi t&uacute;i mủ chứa nhiều vi khuẩn, miệng h&ocirc;i.</p>\r\n<p>Để ph&ograve;ng ngừa bệnh nha chu, cần giữ vệ sinh răng miệng, chải răng đ&uacute;ng c&aacute;ch, ăn những thức ăn mềm.</p>\r\n<p><strong><em>3. L&agrave;m răng giả nếu bị mất răng</em></strong></p>\r\n<p>D&ugrave; răng bị mất v&igrave; bất cứ l&yacute; do g&igrave;, th&igrave; người cao tuổi cũng n&ecirc;n đến nha sĩ để kh&aacute;m v&agrave; phục h&igrave;nh răng sau đ&oacute; 1 th&aacute;ng. Nếu để l&acirc;u, c&aacute;c răng sẽ bị x&ocirc; lệch, l&agrave;m mất khoảng trống của răng đ&atilde; mất, đồng thời g&acirc;y x&aacute;o trộn khớp cắn, kh&oacute; chải sạch răng.</p>\r\n<p>Khi đ&atilde; c&oacute; răng giả, n&ecirc;n chăm s&oacute;c ch&uacute;ng thật kỹ, chải răng hằng ng&agrave;y như răng thật. Nếu d&ugrave;ng răng giả kiểu th&aacute;o lắp, n&ecirc;n th&aacute;o ra l&uacute;c nghỉ ngơi hoặc đi ngủ để ni&ecirc;m mạc ở h&agrave;m được tho&aacute;ng, m&aacute;u lưu th&ocirc;ng dễ d&agrave;ng. H&agrave;m giả th&aacute;o ra n&ecirc;n được vệ sinh sạch sẽ, ng&acirc;m v&agrave;o một ly nước nguội c&oacute; nắp đậy, tốt nhất l&agrave; ly thủy tinh./.</p>', 'active', '/uploads/article/V2558yjiFe.jpeg', NULL, '2020-06-17 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(9, 5, 'BẠN CÓ ĐANG CHĂM SÓC RĂNG MIỆNG ĐÚNG CÁCH', '<h2><strong>1. Tầm quan trọng của việc chăm s&oacute;c răng miệng h&agrave;ng ng&agrave;y</strong></h2>\r\n<p>Miệng ch&iacute;nh l&agrave; cổng v&agrave;o của cơ thể, do đ&oacute; t&igrave;nh trạng răng miệng ảnh hưởng rất lớn đến sức khỏe của bạn. Một chế độ&nbsp;<a href=\"https://careplusvn.com/vi/cham-soc-rang-mieng\">chăm s&oacute;c răng miệng</a>&nbsp;tốt sẽ mang lại cho bạn những lợi &iacute;ch tuyệt vời như:</p>\r\n<h3>1.1. Sở Hữu H&agrave;m Răng Trắng S&aacute;ng, Ngăn Ngừa Nguy Cơ Bị S&acirc;u Răng</h3>\r\n<p>Chăm s&oacute;c răng miệng đ&uacute;ng c&aacute;ch gi&uacute;p loại bỏ c&aacute;c mảng b&aacute;m v&agrave; sự t&iacute;ch tụ của vi khuẩn trong khoang miệng hiệu quả. Nhờ đ&oacute; gi&uacute;p ph&ograve;ng tr&aacute;nh c&aacute;c nguy cơ g&acirc;y mất răng như s&acirc;u răng, vi&ecirc;m nướu, vi&ecirc;m nha chu... Đặc biệt, việc chăm s&oacute;c răng miệng h&agrave;ng ng&agrave;y c&ograve;n c&oacute; t&aacute;c dụng hạn chế t&igrave;nh trạng răng bị ố v&agrave;ng do sự b&aacute;m m&agrave;u của c&aacute;c thực phẩm bạn d&ugrave;ng h&agrave;ng ng&agrave;y.&nbsp;</p>\r\n<p><img src=\"https://nhakhoalutadent.com/files/images/DSC_1444.jpg\" alt=\"\" /></p>\r\n<p><em>Sở hữu h&agrave;m răng khỏe, đẹp gi&uacute;p bạn tự tin hơn trong giao tiếp với mọi người.</em></p>\r\n<h3>1.2. Ăn Nhai Tốt Hơn</h3>\r\n<p>Chất lượng răng ảnh hưởng rất lớn đến khả năng ăn nhai của bạn. Tr&ecirc;n thực tế, t&igrave;nh trạng răng bị suy yếu vẫn c&oacute; thể xảy ra với những người trẻ nếu kh&ocirc;ng chăm s&oacute;c răng tốt. Do đ&oacute; bạn cần ch&uacute; &yacute; đến c&aacute;ch vệ sinh răng miệng đ&uacute;ng c&aacute;ch để thoải m&aacute;i thưởng thức những loại thực phẩm m&agrave; m&igrave;nh y&ecirc;u th&iacute;ch về l&acirc;u d&agrave;i.</p>\r\n<h3>1.3. Ổn Định Hoặc Ph&ograve;ng Tr&aacute;nh C&aacute;c Bệnh L&yacute; Nguy Hiểm</h3>\r\n<p><strong>Bệnh tiểu đường:</strong></p>\r\n<p>C&aacute;c vấn đề về nướu, đặc biệt l&agrave; vi&ecirc;m nha chu c&oacute; mối li&ecirc;n hệ khăng kh&iacute;t với bệnh tiểu đường. Theo đ&oacute;, những người bị tiểu đường thường c&oacute; nguy cơ bị vi&ecirc;m nha chu cao. Đồng thời vi&ecirc;m nha chu sẽ l&agrave;m bệnh tiểu đường phức tạp hơn nữa.</p>\r\n<p>Giải th&iacute;ch về mối quan hệ hai chiều n&agrave;y, Pamela McClain - chủ tịch của Viện H&agrave;n l&acirc;m Nha khoa Mỹ - cho biết, vi&ecirc;m nha chu sẽ l&agrave;m hạn chế sự chuyển đổi Insulin của cơ thể. Trong khi đ&oacute;, Insulin lại l&agrave; loại hoocmon gi&uacute;p kiểm so&aacute;t lượng đường trong m&aacute;u bằng c&aacute;ch dự trữ đường ở gan v&agrave; chỉ giải ph&oacute;ng đường khi lượng đường trong m&aacute;u thấp hoặc khi cơ thể cần nhiều đường. V&igrave; thế, những người mắc hoặc kh&ocirc;ng mắc bệnh tiểu đường đều cần c&oacute; chế độ chăm s&oacute;c răng miệng tốt để tr&aacute;nh l&agrave;m ảnh hưởng đến sự chuyển đổi Insulin của cơ thể, gi&uacute;p ổn định lượng đường trong m&aacute;u tốt hơn.</p>\r\n<p><strong>Bệnh tim:</strong></p>\r\n<p>Về l&yacute; thuyết, t&igrave;nh trạng vi&ecirc;m răng miệng c&oacute; thể g&acirc;y ra vi&ecirc;m trong mạch m&aacute;u. Từ đ&oacute;, c&aacute;c mạch m&aacute;u bị vi&ecirc;m sẽ l&agrave;m giảm sự di chuyển của c&aacute;c tế b&agrave;o m&aacute;u giữa tim v&agrave; phần c&ograve;n lại của cơ thể. Do đ&oacute; khiến người người bệnh dễ bị tăng huyết &aacute;p.</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, c&aacute;c mảng b&aacute;m chất b&eacute;o cũng c&oacute; khả năng ph&aacute; vỡ th&agrave;nh mạch m&aacute;u v&agrave; di chuyển đến tim hoặc n&atilde;o. Đ&acirc;y l&agrave; một trong những nguy&ecirc;n nh&acirc;n g&acirc;y ra những cơn đau tim hoặc đột quỵ.</p>\r\n<p><strong>C&aacute;c bệnh kh&aacute;c:</strong></p>\r\n<p>- Vi&ecirc;m khớp dạng thấp: Mối li&ecirc;n quan giữa bệnh răng miệng v&agrave; vi&ecirc;m khớp dạng thấp đ&atilde; được nghi&ecirc;n cứu v&agrave;o đầu những năm 1820. Theo đ&oacute;, chữa dứt c&aacute;c bệnh về răng miệng c&oacute; thể l&agrave;m giảm đ&aacute;ng kể t&igrave;nh trạng đau cho những bệnh nh&acirc;n vi&ecirc;m khớp dạng thấp .</p>\r\n<p>- Bệnh về phổi: C&aacute;c bệnh về răng miệng c&oacute; thể l&agrave;m tăng số lượng vi khuẩn trong phổi, khiến cho t&igrave;nh trạng&nbsp;<a href=\"https://careplusvn.com/vi/viem-phoi-nguyen-nhan-gay-tu-vong-hang-dau-o-tre-em\">vi&ecirc;m phổi</a>&nbsp;v&agrave; bệnh tắc nghẽn phổi m&atilde;n t&iacute;nh tồi tệ hơn.</p>\r\n<p>- B&eacute;o ph&igrave;: Tương tự như tiểu đường, c&aacute;c vấn đề về răng miệng l&agrave; nguy cơ l&agrave;m tăng lượng mỡ trong m&aacute;u v&agrave; ngược lại.</p>', 'active', '/uploads/article/v4sx9VHGTo.jpeg', NULL, '2020-06-17 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(10, 3, 'Niềng răng thẩm mỹ', '<h2><strong>Niềng răng l&agrave; g&igrave; &amp; n&ecirc;n niềng răng trong trường hợp n&agrave;o?</strong></h2>\r\n<p>Niềng răng thẩm mỹ l&agrave; một trong những dịch vụ nha khoa phổ biến hiện nay. Với phương ph&aacute;p n&agrave;y, c&aacute;c kh&iacute; cụ th&iacute;ch hợp sẽ được gắn cố định hoặc th&aacute;o lắp l&ecirc;n răng để đảm bảo tạo lực k&eacute;o ổn định, gi&uacute;p dịch chuyển v&agrave; sắp xếp răng hiệu quả về đ&uacute;ng vị tr&iacute;.&nbsp;Niềng răng chỉnh nha hiện được chỉ định v&agrave; mang lại hiệu quả cao trong khắc phục c&aacute;c trường hợp khuyết điểm răng sau:</p>\r\n<ul>\r\n<li>Cải thiện t&igrave;nh trạng&nbsp;h&ocirc; m&oacute;m vẩu do răng từ đơn giản đến phức tạp.</li>\r\n<li>Răng mọc lộn xộn, khấp khểnh, sai lệch khớp cắn.</li>\r\n<li>Răng thưa, khoảng c&aacute;ch giữa c&aacute;c răng lớn, c&aacute;ch xa nhau.</li>\r\n<li>Chỉnh h&igrave;nh c&aacute;c răng khểnh mọc lệch.</li>\r\n</ul>\r\n<p><img src=\"https://nhakhoalutadent.com/files/images/DSC_1444.jpg\" alt=\"\" /></p>\r\n<p><em>Niềng răng l&agrave; giải ph&aacute;p gi&uacute;p kh&aacute;ch h&agrave;ng tự tin với nụ cười đều tăm tắp v&agrave; rạng rỡ</em></p>\r\n<h2><strong>C&aacute;c phương ph&aacute;p niềng răng phổ biến v&agrave; hiệu quả</strong></h2>\r\n<p>C&oacute; kh&aacute; nhiều phương ph&aacute;p niềng răng thẩm mỹ được &aacute;p dụng nhằm đ&aacute;p ứng nhu cầu ng&agrave;y c&agrave;ng đa dạng của kh&aacute;ch h&agrave;ng. T&ugrave;y v&agrave;o t&igrave;nh trạng răng, m&oacute;ng muốn v&agrave; điều kiện t&agrave;i ch&iacute;nh của mỗi người m&agrave; b&aacute;c sĩ sẽ cho tư vấn phương ph&aacute;p ph&ugrave; hợp.</p>\r\n<p><strong>&nbsp;Niềng răng mắc c&agrave;i</strong></p>\r\n<p>Niềng răng mắc c&agrave;i l&agrave; phương ph&aacute;p chỉnh nha truyền thống, gắn cố định hệ thống mắc c&agrave;i v&agrave; d&acirc;y cung l&ecirc;n răng, tạo lực chỉnh nha ổn định v&agrave; d&agrave;n trải, dịch chuyển răng về đ&uacute;ng vị tr&iacute; mong muốn. Niềng răng mắc c&agrave;i được ứng dụng rất phổ biến v&agrave; được đ&aacute;nh gi&aacute; cao về hiệu quả, điều m&agrave; c&aacute;c phương ph&aacute;p mới d&ugrave; c&oacute; cải tiến cũng kh&ocirc;ng đ&aacute;p ứng được.</p>\r\n<p>Niềng răng mắc c&agrave;i sử dụng nhiều loại mắc c&agrave;i kh&aacute;c nhau, như mắc c&agrave;i kim loại, mắc c&agrave;i sứ, mắc c&agrave;i tự buộc, mắc c&agrave;i mặt lưỡi.&nbsp;Kh&aacute;ch h&agrave;ng c&oacute; thể lựa chọn phương ph&aacute;p niềng răng thẩm mỹ ph&ugrave; hợp với nhu cầu v&agrave; t&igrave;nh trạng răng miệng của m&igrave;nh để điều trị.</p>\r\n<p><strong>&nbsp;Niềng răng thẩm mỹ kh&ocirc;ng mắc c&agrave;i&nbsp;</strong></p>\r\n<p>Thay v&igrave; hệ thống c&aacute;c mắc c&agrave;i, d&acirc;y cung, phương ph&aacute;p niềng răng kh&ocirc;ng mắc c&agrave;i sử dụng c&aacute;c khay niềng bằng nhựa dẻo trong suốt, &ocirc;m kh&iacute;t v&agrave;o răng, tạo lực dịch chuyển v&agrave; sắp xếp lại vị tr&iacute; c&aacute;c răng lệch lạc. C&aacute;c khay niềng n&agrave;y c&ograve;n c&oacute; thể th&aacute;o lắp dễ d&agrave;ng trong qu&aacute; tr&igrave;nh sử dụng n&ecirc;n rất thuận tiện khi ăn uống, vệ sinh răng miệng.</p>\r\n<p>Nếu đang t&igrave;m kiếm một phương ph&aacute;p niềng răng thẩm mỹ cao v&agrave; k&iacute;n đ&aacute;o khi chỉnh nha th&igrave; niềng răng kh&ocirc;ng mắc c&agrave;i sẽ l&agrave; sự lựa chọn tối ưu cho bạn. Hiện c&oacute; 2 kỹ thuật niềng răng kh&ocirc;ng mắc c&agrave;i l&agrave; niềng răng Invisalign v&agrave; niềng răng Clear Aligner, trong đ&oacute; niềng răng Invisalign được đ&aacute;nh gi&aacute; cao hơn cả về hiệu quả lẫn t&iacute;nh thẩm mỹ.</p>\r\n<p><strong>Niềng răng kh&iacute; cụ th&aacute;o lắp</strong></p>\r\n<p>Vẫn được liệt k&ecirc; trong danh s&aacute;ch của phương ph&aacute;p niềng răng thẩm mỹ nhưng niềng răng với kh&iacute; cụ th&aacute;o lắp kh&ocirc;ng được &aacute;p dụng phổ biến. Phương ph&aacute;p n&agrave;y thường được chỉ định khi muốn chỉnh nha duy tr&igrave; sau thời gian đeo niềng hoặc niềng răng cho trẻ em trong giai đoạn từ 8 &ndash; 12 tuổi.</p>\r\n<h2><strong>Ưu điểm nổi bật của niềng răng l&agrave; g&igrave;?</strong></h2>\r\n<p>&nbsp;<strong>Đa dạng c&aacute;c phương ph&aacute;p niềng răng thẩm mỹ</strong></p>\r\n<p>Hiện c&oacute; đa dạng c&aacute;c phương ph&aacute;p niềng răng được &aacute;p dụng nhằm đ&aacute;p ứng nhu cầu chỉnh nha chuy&ecirc;n biệt của từng kh&aacute;ch h&agrave;ng. Hơn nữa, với sự cải tiến về chất liệu kh&iacute; cụ, với niềng răng kỹ thuật mới, kh&aacute;ch h&agrave;ng c&oacute; thể ho&agrave;n to&agrave;n tự tin trong suốt qu&aacute; tr&igrave;nh chỉnh nha m&agrave; kh&ocirc;ng lo bị ph&aacute;t hiển.</p>\r\n<p><strong>Hiệu quả chỉnh răng cao, kh&ocirc;ng lo sai lệch</strong></p>\r\n<p>Niềng răng thẩm mỹ c&ograve;n được đ&aacute;nh gi&aacute; cao về t&iacute;nh hiệu quả, gi&uacute;p nắn chỉnh, sắp xếp răng về đ&uacute;ng vị tr&iacute; trong thời gian quy định hoặc nhanh hơn. Hơn nữa, to&agrave;n bộ qu&aacute; tr&igrave;nh n&agrave;y cũng được t&iacute;nh to&aacute;n ch&iacute;nh x&aacute;c v&agrave; theo d&otilde;i kỹ để đảm bảo kh&ocirc;ng c&oacute; trường hợp bị sai lệch.</p>', 'active', '/uploads/article/grPmUhswqI.jpeg', NULL, '2020-06-18 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(11, 3, 'Trồng răng implant', '<h2><strong>Ai th&iacute;ch hợp trồng răng implant?</strong></h2>\r\n<p>Trồng răng implant l&agrave; h&igrave;nh thức đặt trụ&nbsp;bằng hợp kim Titanium v&agrave;o b&ecirc;n trong xương h&agrave;m ở v&ugrave;ng đ&atilde; mất răng nhằm mục đ&iacute;ch thay thế ch&acirc;n răng rồi&nbsp;bọc m&atilde;o răng sứ l&ecirc;n trụ implant th&ocirc;ng qua&nbsp;khớp nối Abutment. Sau khi cấy implant bạn sẽ sở hữu&nbsp;một chiếc răng ho&agrave;n chỉnh, đầy đủ ch&acirc;n răng v&agrave; th&acirc;n răng như răng&nbsp;thật.</p>\r\n<p><img src=\"https://nhakhoalutadent.com/files/images/DSC_2673.jpg\" alt=\"\" /></p>\r\n<p><em>Trồng răng implant l&agrave; h&igrave;nh thức cấy gh&eacute;p trực tiếp trụ implant v&agrave;o trong xương h&agrave;m</em></p>\r\n<p>Trồng răng implant c&oacute; khả năng phục h&igrave;nh răng mang lại hiệu quả cải thiện tốt như mong đợi cho kh&aacute;ch h&agrave;ng trong c&aacute;c trường hợp sau:</p>\r\n<ul>\r\n<li>Mất 1 răng hoặc mất nhiều răng.</li>\r\n<li>Mất răng to&agrave;n h&agrave;m, c&oacute; thể 1 h&agrave;m hoặc cả 2 h&agrave;m.</li>\r\n<li>Mới bị mất răng hoặc đ&atilde; mất răng l&acirc;u ng&agrave;y.</li>\r\n</ul>\r\n<h2><strong>Tại sao n&ecirc;n thực hiện trồng răng implant?</strong></h2>\r\n<p>Hiện nay,&nbsp;phương ph&aacute;p&nbsp;trồng răng&nbsp;implant được rất đ&ocirc;ng kh&aacute;ch h&agrave;ng&nbsp;lựa chọn bởi ngo&agrave;i việc phục h&igrave;nh răng bị mất hiệu quả,&nbsp;kỹ thuật n&agrave;y&nbsp;c&ograve;n mang nhiều ưu điểm vượt trội,&nbsp;khắc phục ho&agrave;n to&agrave;n những thiếu s&oacute;t của biện ph&aacute;p trồng răng truyền thống.</p>\r\n<p><strong>&nbsp;Mang t&iacute;nh thẩm mỹ ho&agrave;n hảo, giống như răng thật</strong></p>\r\n<p>Răng implant sau khi phục h&igrave;nh c&oacute; m&agrave;u sắc trắng s&aacute;ng, độ b&oacute;ng, h&igrave;nh d&aacute;ng như răng thật, ho&agrave;n to&agrave;n sẽ kh&ocirc;ng ai ph&aacute;t hiện được khi nh&igrave;n bằng mắt thường. Bạn c&oacute; thể&nbsp; ăn nhai b&igrave;nh thường m&agrave; kh&ocirc;ng gặp phải vấn đề g&igrave;.</p>\r\n<p><strong>&nbsp;Kh&ocirc;ng đau, hạn chế x&acirc;m lấn, đảm bảo an to&agrave;n</strong></p>\r\n<p>To&agrave;n bộ quy tr&igrave;nh cấy gh&eacute;p implant được tiến h&agrave;nh trong điều kiện v&ocirc; tr&ugrave;ng, c&oacute; sự hỗ trợ của nhiều m&aacute;y m&oacute;c,&nbsp;thiết bị hiện đại v&agrave; c&ocirc;ng nghệ 3D tiến tiến,&nbsp;gi&uacute;p x&aacute;c định ch&iacute;nh x&aacute;c t&igrave;nh trạng mất răng, vị tr&iacute; cắm trụ&nbsp;ph&ugrave; hợp.</p>\r\n<p>Răng implant tồn tại đ&ocirc;̣c l&acirc;̣p như một chiếc răng thật tr&ecirc;n cung h&agrave;m, kh&ocirc;ng g&acirc;y ảnh hưởng đến c&aacute;c răng kh&aacute;c v&agrave; cũng kh&ocirc;ng cần t&aacute;c động đến cấu tr&uacute;c răng như những&nbsp;phương ph&aacute;p kh&aacute;c.</p>\r\n<p><strong>&nbsp;Ngăn&nbsp;chặn&nbsp;t&igrave;nh trạng ti&ecirc;u xương h&agrave;m</strong></p>\r\n<p>Tiến h&agrave;nh cấy gh&eacute;p implant vừa c&oacute; thể ngăn chặn t&igrave;nh trạng&nbsp;ti&ecirc;u xương&nbsp;diễn ra (dẫn đến tụt nướu, teo quay h&agrave;m khiến m&aacute; h&oacute;p, khu&ocirc;n mặt gi&agrave; hơn tuổi), vừa gi&uacute;p bạn tr&aacute;nh được những vấn đề kh&ocirc;ng mong muốn, ảnh hưởng đến thẩm mỹ v&agrave; sức khỏe răng miệng về sau.</p>\r\n<p>&nbsp;<strong>Tuổi thọ d&agrave;i l&acirc;u, khả năng chịu lực tốt</strong></p>\r\n<p>Trụ răng Implant được thiết kế đặc biệt v&agrave; chế tạo từ vật liệu bền vững l&agrave; Titanium, c&oacute; khả năng t&iacute;ch hợp tốt với xương ổ răng&nbsp;c&ugrave;ng&nbsp;nướu một c&aacute;ch nhanh ch&oacute;ng, d&ugrave;&nbsp;sử dụng trong thời gian d&agrave;i cũng kh&ocirc;ng biến chất, kh&ocirc;ng bị oxy h&oacute;a trong m&ocirc;i trường&nbsp;khoang miệng. Răng&nbsp;Implant&nbsp;kh&ocirc;ng g&acirc;y k&iacute;ch ứng,&nbsp;phản ứng đ&agrave;o thải&nbsp;hay t&aacute;c dụng phụ với cơ thể, lại c&oacute; thể tồn tại l&acirc;u d&agrave;i, thậm ch&iacute; l&agrave; trọn đời nếu chăm s&oacute;c v&agrave; giữ g&igrave;n đ&uacute;ng c&aacute;ch.</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, răng implant c&oacute; độ cứng cao, khả năng chịu lực tốt, đảm bảo khả năng ăn nhai b&igrave;nh thường như răng thật sau khi trồng.</p>', 'active', '/uploads/article/qwkLJuuEOX.jpeg', NULL, '2020-06-18 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(12, 3, 'Mặt dán sứ veneer', '<h2><img src=\"/public/uploads/news/hieufood2_s.png\" alt=\"\" /><strong><img src=\"/public/uploads/news/lehung.jpg\" alt=\"\" /><img src=\"/public/storage/uploads/news/79832766_2279867485450908_3950728974350417920_n.jpg\" alt=\"\" /></strong><strong>V&igrave; sao mặt d&aacute;n sứ veneer được nhiều người y&ecirc;u th&iacute;ch?</strong></h2>\r\n<p>Mặt d&aacute;n sứ Veneer l&agrave; một lớp răng sứ mỏng cứng chắc, c&oacute; m&agrave;u sắc trắng đẹp tự nhi&ecirc;n. Để phục h&igrave;nh, mặt d&aacute;n sứ Veneer sẽ được gắn l&ecirc;n mặt trước của răng gi&uacute;p che đi c&aacute;c khuyết điểm về mặt thẩm mỹ.</p>\r\n<p>L&agrave;m mặt d&aacute;n sứ Veneer được &aacute;p dụng cho c&aacute;c trường hợp như:</p>\r\n<ul>\r\n<li>Răng bị ố v&agrave;ng, xỉn m&agrave;u do nhiễm thuốc kh&aacute;ng sinh</li>\r\n<li>Răng mọc thưa, lệch lạc</li>\r\n<li>Răng ngắn, k&iacute;ch thước c&aacute;c răng kh&ocirc;ng đều</li>\r\n<li>Răng&nbsp;bị bể, mẻ, vỡ</li>\r\n<li>Răng&nbsp;bị m&ograve;n men</li>\r\n</ul>\r\n<p><img src=\"https://nhakhoalutadent.com/files/images/DSC_1444.jpg\" alt=\"\" /></p>\r\n<p><em>Hoa hậu - Si&ecirc;u mẫu quốc gia ch&acirc;u &Aacute; đ&atilde; tin tưởng chọn lựa Nha khoa để c&oacute; nụ cười rạng rỡ</em></p>\r\n<h2><strong>Ưu điểm vượt trội của mặt d&aacute;n sứ veneer</strong></h2>\r\n<p>Với việc khắc phục những nhược điểm của c&aacute;c phương ph&aacute;p bọc răng sứ truyền thống,&nbsp;kể cả qu&aacute; tr&igrave;nh thực hiện, t&iacute;nh thẩm mỹ, độ bền đẹp, linh hoạt, mức độ m&agrave;i răng....lắp mặt d&aacute;n sứ Veneer&nbsp;đang l&agrave; xu hướng thẩm mỹ răng đang được rất nhiều người y&ecirc;u th&iacute;ch. Dưới đ&acirc;y l&agrave; những lợi &iacute;ch nổi bật của&nbsp;mặt d&aacute;n sứ Veneer:</p>\r\n<p><strong>&nbsp;Phục h&igrave;nh răng đẹp như thật</strong></p>\r\n<p>Mặt d&aacute;n sứ Veneer 3D được chế tạo từ khối sứ nguy&ecirc;n chất tr&ecirc;n c&ocirc;ng nghệ CAD/CAM 3D,&nbsp;c&oacute; m&agrave;u trắng, độ trong, b&oacute;ng v&agrave; độ&nbsp;&ldquo;gợn&rdquo; tự nhi&ecirc;n tương tự ng&agrave; răng. Đ&acirc;y l&agrave; ưu điểm tuyệt vời của mặt d&aacute;n sứ Veneer 3D m&agrave; chưa c&oacute; loại răng sứ n&agrave;o hiện nay c&oacute; thể vượt qua được, mang đến h&agrave;m răng&nbsp;đẹp, sống động, tự nhi&ecirc;n hơn cả răng thật.</p>\r\n<p><strong>&nbsp;Giải quyết hiệu quả răng xấu&nbsp;</strong></p>\r\n<p>Với thiết kế đặc biệt,&nbsp;mặt d&aacute;n sứ Veneer 3D rất th&iacute;ch hợp với răng cửa, c&oacute; thể khắc phục một c&aacute;ch hiệu quả những khiếm khuyết của răng xấu như: Răng ố v&agrave;ng, xỉn m&agrave;u, nhiễm kh&aacute;ng sinh, m&ograve;n men răng, sứt mẻ, lệch, vẹo,&nbsp;c&aacute;c răng kh&ocirc;ng đều nhau về chiều d&agrave;i, bề ngang hoặc bản th&acirc;n mỗi răng lại kh&ocirc;ng c&acirc;n đối...</p>\r\n<p><strong>&nbsp;Hạn chế m&agrave;i răng tối đa</strong></p>\r\n<p>Kh&ocirc;ng chỉ chiếm trọn sự tin y&ecirc;u của đ&ocirc;ng đảo kh&aacute;ch h&agrave;ng v&agrave; b&aacute;c sĩ chuy&ecirc;n khoa bởi hiệu quả, t&iacute;nh thẩm mỹ, m&agrave;&nbsp;mặt d&aacute;n sứ Veneer 3D c&ograve;n được đ&aacute;nh gi&aacute; cao về việc hạn chế m&agrave;i răng tối đa. &nbsp;Khi lắp mặt d&aacute;n&nbsp;răng&nbsp;sứ Veneer 3D&nbsp;bạn chỉ phải thực hiện m&agrave;i một phần men răng ở mặt trước của răng v&agrave; tỷ lệ m&agrave;i rất &iacute;t, khoảng 1/3 so với l&agrave;m răng th&ocirc;ng thường, gi&uacute;p&nbsp;bảo tồn răng thật tối đa,&nbsp;kh&ocirc;ng cần phải lấy tủy, răng kh&ocirc;ng bị &ecirc; buốt kh&oacute; chịu.</p>\r\n<p><strong>&nbsp;Độ bền cao</strong></p>\r\n<p>Mặt d&aacute;n sứ Veneer 3D c&oacute; tuổi thọ cao hơn c&aacute;c loại răng sứ kh&aacute;c&nbsp;trung b&igrave;nh từ 10 &ndash; 15 năm v&agrave; nếu được phục h&igrave;nh bởi đội ngũ b&aacute;c sĩ giỏi, chăm s&oacute;c đ&uacute;ng c&aacute;ch th&igrave; c&oacute; thể duy tr&igrave; l&acirc;u hơn. C&ugrave;ng với đ&oacute;, chất liệu kết d&iacute;nh đặc biệt, kh&ocirc;ng dễ bị h&oacute;a lỏng n&ecirc;n mặt d&aacute;n sứ Veneer 3D b&aacute;m chắc kh&aacute; tốt&nbsp;tr&ecirc;n th&acirc;n răng, đảm bảo sau khi phục h&igrave;nh răng kh&ocirc;ng bị k&ecirc;nh hở, kh&ocirc;ng dễ bị bong hay bật khi nhai gắn, chải răng.</p>\r\n<p><strong>&nbsp;Thoải m&aacute;i ăn nhai</strong></p>\r\n<p>Do mặt d&aacute;n&nbsp;răng&nbsp;sứ Veneer 3D kh&aacute; mỏng (khoảng 0,5 &ndash; 0,6 mm), v&igrave; vậy trong qu&aacute; tr&igrave;nh ăn nhai bạn ho&agrave;n to&agrave;n kh&ocirc;ng&nbsp;cảm thấy vướng v&iacute;u, cộm cấn hay kh&oacute; chịu. Ngo&agrave;i ra, bởi&nbsp;được chế tạo từ c&aacute;c khối sứ nguy&ecirc;n chất Lithium Desilicate hay Zirconium, n&oacute;&nbsp;c&oacute; thể chịu được &aacute;p lực nhai cắn của răng cửa lớn, gi&uacute;p người d&ugrave;ng thoải m&aacute;i thưởng thức những m&oacute;n ăn y&ecirc;u th&iacute;ch.</p>', 'active', '/uploads/article/KOW9ZvSHf3.jpeg', NULL, '2020-06-18 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `_lft` int(11) NOT NULL DEFAULT '0',
  `_rgt` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `is_home` text,
  `display` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `_lft`, `_rgt`, `name`, `status`, `created`, `created_by`, `modified`, `modified_by`, `is_home`, `display`) VALUES
(1, NULL, 1, 2, 'Ưu đãi', 'active', '2019-05-04 00:00:00', 'admin', '2019-07-09 09:41:27', 'admin', 'yes', 'list'),
(2, NULL, 3, 4, 'Bảng giá', 'active', '2019-05-04 00:00:00', 'admin', '2019-07-09 08:42:58', 'admin', 'yes', 'list'),
(3, NULL, 5, 6, 'Dịch vụ', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-15 15:04:33', 'admin', 'yes', 'list'),
(4, NULL, 7, 8, 'Khách hàng', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-15 15:04:30', 'admin', 'yes', 'list'),
(5, NULL, 9, 10, 'Chăm sóc răng cơ bản', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-12 00:00:00', 'admin', 'yes', 'list'),
(6, NULL, 11, 12, 'Chăm sóc răng cho bé', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-15 15:04:38', 'admin', 'yes', 'grid'),
(7, NULL, 13, 14, 'Chăm sóc răng cho mẹ', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-15 15:04:36', 'admin', 'yes', 'list'),
(8, NULL, 15, 16, 'Chăm sóc răng người lớn tuổi', 'active', '2019-05-12 00:00:00', 'admin', '2020-06-12 00:00:00', 'hailan', 'yes', 'list'),
(9, NULL, 17, 18, 'Giới thiệu', 'active', '2020-06-12 00:00:00', 'hailan', NULL, NULL, 'yes', 'list');

-- --------------------------------------------------------

--
-- Table structure for table `cate_news`
--

DROP TABLE IF EXISTS `cate_news`;
CREATE TABLE IF NOT EXISTS `cate_news` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `status` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(4) NOT NULL DEFAULT '0',
  `display` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_models__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cate_news`
--

INSERT INTO `cate_news` (`id`, `name`, `slug`, `_lft`, `_rgt`, `parent_id`, `status`, `is_home`, `display`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(8, 'root', NULL, 1, 14, NULL, 'active', 0, NULL, '2019-07-11 09:54:29', 'admin', '2019-07-11 09:54:29', 'admin'),
(9, 'Tư vấn mua sản phẩm', 'tu-van-mua-san-pham', 2, 9, 8, 'active', 0, NULL, '2019-07-11 10:04:27', 'admin', '2019-09-15 13:09:47', 'admin'),
(11, 'Thủ thuật', 'thu-thuat', 12, 13, 8, 'active', 0, NULL, '2019-07-11 10:07:07', 'admin', '2019-09-15 13:09:15', 'admin'),
(12, 'Điện thoại', 'dien-thoai', 7, 8, 9, 'active', 0, NULL, '2019-07-11 15:07:19', 'admin', '2019-09-15 13:09:27', 'admin'),
(13, 'Laptop', 'laptop', 3, 4, 9, 'active', 0, NULL, '2019-07-23 10:07:04', 'admin', '2019-09-16 07:09:25', 'admin'),
(16, 'Mạng xã hội', 'mang-xa-hoi', 10, 11, 8, 'active', 0, NULL, '2019-07-26 23:07:47', 'admin', '2019-09-15 13:09:34', 'admin'),
(17, 'Phụ kiện', 'phu-kien', 5, 6, 9, 'active', 0, NULL, '2019-09-09 10:09:38', 'admin', '2019-09-15 13:09:51', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `cate_product`
--

DROP TABLE IF EXISTS `cate_product`;
CREATE TABLE IF NOT EXISTS `cate_product` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `status` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(4) NOT NULL DEFAULT '0',
  `display` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_models__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cate_product`
--

INSERT INTO `cate_product` (`id`, `name`, `slug`, `_lft`, `_rgt`, `parent_id`, `status`, `is_home`, `display`, `meta_description`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(8, 'root', NULL, 1, 60, NULL, 'active', 0, NULL, '', '2019-07-11 09:54:29', 'admin', '2019-07-11 09:54:29', 'admin'),
(9, 'COHIBA MADURO 5', 'cohiba-maduro-5', 7, 8, 19, 'active', 0, NULL, '', '2019-07-11 10:04:27', 'admin', '2019-09-19 14:09:59', 'admin'),
(11, 'EXQUISITOS', 'exquisitos', 13, 14, 19, 'active', 0, NULL, '', '2019-07-11 10:07:07', 'admin', '2019-09-19 14:09:44', 'admin'),
(12, 'HOYO', 'hoyo', 15, 16, 19, 'active', 0, NULL, '', '2019-07-11 15:07:19', 'admin', '2019-09-19 14:09:11', 'admin'),
(13, 'H.UPMAN', 'hupman', 19, 20, 19, 'active', 0, NULL, '', '2019-07-23 10:07:04', 'admin', '2019-09-19 14:09:55', 'admin'),
(16, 'COHIBA SIGLO', 'cohiba-siglo', 5, 6, 19, 'active', 0, NULL, '', '2019-07-26 23:07:47', 'admin', '2019-09-19 14:09:35', 'admin'),
(17, 'COHIBA OTHER', 'cohiba-other', 9, 10, 19, 'active', 0, NULL, '', '2019-09-09 10:09:38', 'admin', '2019-09-19 14:09:25', 'admin'),
(18, 'COHIBA BEHIKE', 'cohiba-behike', 3, 4, 19, 'active', 0, NULL, '', '2019-09-14 02:09:28', 'admin', '2019-09-19 14:09:09', 'admin'),
(19, 'XÌ GÀ CUBA', 'xi-ga-cuba', 2, 41, 8, 'active', 0, NULL, 'Chuyên cung cấp các loại Xì Gà Cu Ba chính hãng.', '2019-09-14 02:09:40', 'admin', '2019-10-18 09:10:33', 'admin'),
(20, 'BOLIVAR', 'bolivar', 23, 24, 19, 'active', 0, NULL, '', '2019-09-14 02:09:22', 'admin', '2019-09-19 14:09:33', 'admin'),
(21, 'BOSSNER', 'bossner', 21, 22, 19, 'active', 0, NULL, '', '2019-09-14 02:09:37', 'admin', '2019-09-19 14:09:41', 'admin'),
(22, 'JOSPEDRA', 'jospedra', 25, 26, 19, 'active', 0, NULL, '', '2019-09-19 14:09:26', 'admin', NULL, NULL),
(23, 'MONTECRISTO', 'montecristo', 27, 28, 19, 'active', 0, NULL, '', '2019-09-19 14:09:32', 'admin', NULL, NULL),
(24, 'NON-CUBA', 'non-cuba', 17, 18, 19, 'active', 0, NULL, '', '2019-09-19 14:09:37', 'admin', NULL, NULL),
(25, 'PARTAGAS', 'partagas', 29, 30, 19, 'active', 0, NULL, '', '2019-09-19 14:09:44', 'admin', NULL, NULL),
(26, 'PUNCH', 'punch', 31, 32, 19, 'active', 0, NULL, '', '2019-09-19 14:09:49', 'admin', NULL, NULL),
(27, 'ROBAINA', 'robaina', 33, 34, 19, 'active', 0, NULL, '', '2019-09-19 14:09:03', 'admin', NULL, NULL),
(28, 'ROMEO Y JULIETA', 'romeo-y-julieta', 11, 12, 19, 'active', 0, NULL, NULL, '2019-09-19 14:09:09', 'admin', '2019-10-22 14:10:24', 'admin'),
(29, 'TRINIDAD', 'trinidad', 35, 36, 19, 'active', 0, NULL, NULL, '2019-09-19 14:09:15', 'admin', '2019-10-22 14:10:12', 'admin'),
(30, 'XÌ GÀ BÁN LẺ', 'xi-ga-ban-le', 37, 38, 19, 'active', 0, NULL, '', '2019-09-19 14:09:21', 'admin', NULL, NULL),
(31, 'XÌ GÀ MINI', 'xi-ga-mini', 39, 40, 19, 'active', 0, NULL, '', '2019-09-19 14:09:27', 'admin', NULL, NULL),
(33, 'RƯỢU VANG', 'ruou-vang', 42, 43, 8, 'active', 0, NULL, '', '2019-09-19 14:09:38', 'admin', NULL, NULL),
(34, 'PHỤ KIỆN XÌ GÀ', 'phu-kien-xi-ga', 44, 59, 8, 'active', 0, NULL, '', '2019-09-19 14:09:47', 'admin', NULL, NULL),
(35, 'BẬT LỬA', 'bat-lua', 47, 48, 34, 'active', 0, NULL, NULL, '2019-09-19 14:09:56', 'admin', '2019-10-22 14:10:50', 'admin'),
(36, 'DAO CẮT', 'dao-cat', 49, 50, 34, 'active', 0, NULL, NULL, '2019-09-19 14:09:04', 'admin', '2019-10-22 14:10:13', 'admin'),
(37, 'GẠT TÀN', 'gat-tan', 51, 52, 34, 'active', 0, NULL, NULL, '2019-09-19 14:09:12', 'admin', '2019-10-22 14:10:21', 'admin'),
(38, 'SET DỤNG CỤ', 'set-dung-cu', 53, 54, 34, 'active', 0, NULL, NULL, '2019-09-19 14:09:25', 'admin', '2019-10-22 14:10:49', 'admin'),
(40, 'TỦ ĐIỆN XÌ GÀ', 'tu-dien-xi-ga', 55, 56, 34, 'active', 0, NULL, '', '2019-09-19 14:09:40', 'admin', NULL, NULL),
(41, 'TỦ GIỮ ẨM HUMIDOR', 'tu-giu-am-humidor', 57, 58, 34, 'active', 0, NULL, '', '2019-09-19 14:09:49', 'admin', NULL, NULL),
(42, 'TẨU XÌ GÀ', NULL, 45, 46, 34, 'active', 0, NULL, NULL, '2020-06-26 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_name` varchar(50) NOT NULL DEFAULT '0',
  `image` varchar(50) NOT NULL DEFAULT '0',
  `comment` varchar(3000) NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `rate` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT '0',
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `guest_name`, `image`, `comment`, `status`, `rate`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(2, 'Thúy Trâm', '/uploads/comments/578vUR4bZc.jpeg', '<p>Nh&igrave;n chung m&igrave;nh rất h&agrave;i l&ograve;ng về cả gi&aacute; cả lẫn chất lượng sản phẩm. N&ecirc;n m&igrave;nh đ&aacute;nh gi&aacute; 5 sao. Tuy nhi&ecirc;n, mong l&agrave; cửa h&agrave;ng n&ecirc;n l&agrave;m việc lại với b&ecirc;n đối t&aacute;c giao h&agrave;ng. Lần n&agrave;y l&agrave; giao h&agrave;ng nhanh chịu tr&aacute;ch nhiệm giao h&agrave;ng với đơn của m&igrave;nh. Th&aacute;i độ phục vụ thiếu chuy&ecirc;n nghiệp. Mong b&ecirc;n bạn xem x&eacute;t vấn đề n&agrave;y.</p>', 'active', 5, 'admin', '2019-06-12 17:06:20', 'admin', '2020-06-24 00:00:00'),
(3, 'Bảo Trâm', '/uploads/comments/7upRlMKFFx.jpeg', '<p>H&agrave;ng đ&uacute;ng chuẩn model VN/A, H&agrave;ng nguy&ecirc;n seal, chưa k&iacute;ch hoạt. Giao h&agrave;ng 2h ổn. chưa tới 2 tiếng đ&atilde; tới nơi. X&agrave;i được 2 ng&agrave;y chưa thấy vấn đề g&igrave;.</p>', 'active', 5, 'admin', '2019-06-12 17:06:03', 'admin', '2020-06-24 00:00:00'),
(11, 'Võ Yến Vy', '/uploads/comments/xs0gDpMmIo.jpeg', '<p>M&aacute;y mới mua được 1 th&aacute;ng rất &iacute;t khi d&ugrave;ng đến m&agrave; một ng&agrave;y đẹp trời đen x&igrave; m&agrave;n h&igrave;nh, sạc kh&ocirc;ng v&agrave;o. Gọi l&ecirc;n tiki khiếu nại th&igrave; được hướng dẫn mang ra địa chỉ bảo h&agrave;nh của h&atilde;ng apple để bảo h&agrave;nh. Mang ra bảo h&agrave;nh th&igrave; được b&aacute;o lỗi phần cứng đ&atilde; gửi Miền Nam xử l&yacute;.</p>', 'active', 1, 'admin', '2019-07-18 10:07:14', 'admin', '2020-06-24 00:00:00'),
(12, 'Kim Dung', '/uploads/comments/Sgc0XZd61D.jpeg', '<p>Nội dung đ&aacute;nh gi&aacute; sản phẩm</p>', 'active', 4, 'admin', '2020-06-12 00:00:00', 'admin', '2020-06-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dentists`
--

DROP TABLE IF EXISTS `dentists`;
CREATE TABLE IF NOT EXISTS `dentists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `network` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `dentists`
--

INSERT INTO `dentists` (`id`, `position`, `name`, `network`, `content`, `status`, `thumb`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'Trưởng phòng', 'Nguyễn Văn A', 'a:4:{s:6:\"linkin\";N;s:8:\"facebook\";s:36:\"https://www.facebook.com/lehung3011/\";s:7:\"twitter\";N;s:6:\"google\";N;}', '<p>dasd</p>', 'active', '/uploads/dentists/C9J1erdUkV.jpeg', '2020-06-12 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin'),
(2, 'Bác sĩ', 'Phạm Thị H', 'a:4:{s:6:\"linkin\";N;s:8:\"facebook\";s:39:\"https://www.facebook.com/hieuthikimdung\";s:7:\"twitter\";N;s:6:\"google\";N;}', '<p>Th&ocirc;ng tin B&aacute;c sĩ H</p>', 'active', '/uploads/dentists/t93lVgoyjo.jpeg', '2020-06-12 00:00:00', 'admin', '2020-06-24 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(500) NOT NULL DEFAULT '0',
  `answer` longtext,
  `status` varchar(50) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'Răng trắng sau khi tẩy giữ được bao lâu?', '<p>Trung b&igrave;nh việc giữ m&agrave;u trắng của răng sau khi tẩy l&agrave; 2 đến 3 năm nhưng cũng t&ugrave;y thuộc v&agrave;o t&iacute;nh chất men v&agrave; cơ địa của từng người. Đa phần m&agrave;u sắc sẽ giảm nhẹ sau 2 tuần tẩy trắng v&agrave; sẽ ổn định suốt 6 th&aacute;ng đến 1 năm đầu ti&ecirc;n. Một số người c&oacute; thể giữ m&agrave;u đến 3-4 năm. Một số th&oacute;i quen cũng ảnh hưởng đến việc giữ m&agrave;u trắng của răng. Chẳng hạn việc h&uacute;t thuốc l&aacute;, c&agrave;f&ecirc;, tr&agrave;, rượu đỏ v&agrave; c&aacute;c&hellip;</p>\r\n\r\n<p>Trung b&igrave;nh việc giữ m&agrave;u trắng của răng sau khi tẩy l&agrave; 2 đến 3 năm nhưng cũng t&ugrave;y thuộc v&agrave;o t&iacute;nh chất men v&agrave; cơ địa của từng người. Đa phần m&agrave;u sắc sẽ giảm nhẹ sau 2 tuần tẩy trắng v&agrave; sẽ ổn định suốt 6 th&aacute;ng đến 1 năm đầu ti&ecirc;n. Một số người c&oacute; thể giữ m&agrave;u đến 3-4 năm.&nbsp;<br />\r\nMột số th&oacute;i quen cũng ảnh hưởng đến việc giữ m&agrave;u trắng của răng. Chẳng hạn việc h&uacute;t thuốc l&aacute;, c&agrave;f&ecirc;, tr&agrave;, rượu đỏ v&agrave; c&aacute;c loại đồ uống m&agrave;u sẫm&hellip;&nbsp;<br />\r\nViệc giữ vệ sinh răng miệng tốt, thường xuy&ecirc;n l&agrave;m sạch răng với nha sĩ cũng gi&uacute;p việc duy tr&igrave; m&agrave;u trắng của răng. Nếu cần c&oacute; thể sử dụng th&ecirc;m c&aacute;c sản phẩm duy tr&igrave; m&agrave;u răng trắng sau khi tẩy.</p>', 'active', '2019-06-18 03:06:09', 'admin', '2020-06-20 00:00:00', 'admin'),
(2, 'Điều trị với Invisalign mất bao lâu và chi phí là bao nhiêu?', '<p>Thời gian v&agrave; chi ph&iacute; điều trị t&ugrave;y v&agrave;o độ kh&oacute; của từng h&agrave;m răng cần điều trị v&agrave; từ đ&oacute; sẽ quyết định số lượng khay cần thiết. Cứ c&aacute;ch 2 tuần sẽ thay khay một lần. V&igrave; vậy, n&oacute;i chung sẽ c&oacute; khoảng nhiều nhất l&agrave; 48 khay trong loạt khay điều trị, hoặc &iacute;t nhất l&agrave; 12 khay. Chỉ c&oacute; nha sĩ của bạn c&oacute; thể quyết định thời gian bạn cần điều trị.</p>', 'active', '2019-06-18 03:06:29', 'admin', '2020-06-20 00:00:00', 'admin'),
(3, 'Tái tạo răng bằng cách bọc mão là gì?', '<p>Phương ph&aacute;p n&agrave;y được sử dụng trong trường hợp răng bị vỡ lớn, răng chết tủy hoặc c&aacute;c kỹ thuật kh&aacute;c kh&ocirc;ng đ&aacute;p ứng được đ&ograve;i hỏi điều trị. M&atilde;o răng c&oacute; 3 loại: kim loại, composite v&agrave; sứ.</p>\r\n\r\n<p>M&atilde;o sứ l&agrave; phục h&igrave;nh c&oacute; t&iacute;nh thẩm mỹ v&agrave; độ bền cao nhất. C&oacute; thể sử dụng từ 10 đến 20 năm hoặc hơn nữa. M&atilde;o sứ ng&agrave;y c&agrave;ng được ưa chuộng so với m&atilde;o composite v&agrave; kim loại do t&iacute;nh thẫm mỹ cao v&agrave; độ bền của n&oacute;.</p>', 'active', '2019-06-18 03:06:53', 'admin', '2020-06-20 00:00:00', 'admin'),
(4, 'Điều trị bằng Invisalign tôi có phải thường xuyên như cách chỉnh răng bằng mắc cài không?', '<p>Nha sĩ của bạn sẽ l&ecirc;n kế hoạch cho những lần hẹn thường kỳ, th&ocirc;ng thường khoảng 1 th&aacute;ng 1 lần. Đ&acirc;y l&agrave; c&aacute;ch duy nhất nha sĩ của bạn c&oacute; thể chắc chắn rằng việc điều trị đang tiến triển theo đ&uacute;ng kế hoạch.</p>', 'active', '2020-06-20 00:00:00', 'admin', NULL, '0'),
(5, 'Tôi phải chăm sóc Implant của tôi như thế nào?', '<p>Bạn phải chải răng v&agrave; dung chỉ nha khoa để l&agrave;m sạch xung quanh Implant. Đi kh&aacute;m răng định kỳ cũng rất cần thiết cho việc bảo vệ sự khỏe mạnh v&agrave; l&acirc;u bền của Implant.</p>', 'active', '2020-06-20 00:00:00', 'admin', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `list_recall`
--

DROP TABLE IF EXISTS `list_recall`;
CREATE TABLE IF NOT EXISTS `list_recall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(11) NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `list_recall`
--

INSERT INTO `list_recall` (`id`, `phone`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(29, '0963481000', 'inactive', '2019-06-29 22:35:54', 'admin', '2019-06-29 22:35:54', 'admin'),
(31, '0963481001', 'active', '2019-07-01 21:56:59', 'admin', '2019-07-01 21:56:59', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `status` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(4) NOT NULL DEFAULT '0',
  `display` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '_self',
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_models__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `link`, `slug`, `_lft`, `_rgt`, `parent_id`, `status`, `is_home`, `display`, `target`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'root', NULL, NULL, 1, 18, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Trang chủ', '{\"type\":\"link\",\"value\":\"http:\\/\\/lutaden.xyz\\/\"}', NULL, 16, 17, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(3, 'Giới thiệu', '{\"type\":\"category\",\"value\":\"9\"}', NULL, 14, 15, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(4, 'Dịch vụ', '{\"type\":\"category\",\"value\":\"3\"}', NULL, 12, 13, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(5, 'Bảng giá', '{\"type\":\"link\",\"value\":\"\\/bai-viet\\/bang-gia-2.html\"}', NULL, 10, 11, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(6, 'Đặt lịch hẹn', '{\"type\":\"link\",\"value\":\"dat-lich-hen.html\"}', NULL, 8, 9, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(7, 'Ưu đãi', '{\"type\":\"category\",\"value\":\"1\"}', NULL, 6, 7, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(8, 'Liên hệ', '{\"type\":\"link\",\"value\":\"he-thong-chi-nhanh.html\"}', NULL, 4, 5, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(9, 'Câu hỏi thường gặp', '{\"type\":\"link\",\"value\":\"cau-hoi-thuong-gap.html\"}', NULL, 2, 3, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_01_07_073615_create_tagged_table', 1),
(2, '2014_01_07_073615_create_tags_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` smallint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `image_main` varchar(255) DEFAULT NULL,
  `image_extra` varchar(2000) DEFAULT NULL,
  `description_short` text,
  `description_long` text,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `price_default` varchar(50) DEFAULT NULL,
  `price_sale` varchar(50) DEFAULT NULL,
  `sale_start` date DEFAULT NULL,
  `sale_end` date DEFAULT NULL,
  `config` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `slug`, `status`, `category_id`, `image_main`, `image_extra`, `description_short`, `description_long`, `created`, `created_by`, `modified`, `modified_by`, `price_default`, `price_sale`, `sale_start`, `sale_end`, `config`) VALUES
(1, 'Iphone 11', 'iphone-11', 'active', 12, '{\"src\":\"\\/upload\\/1\\/product\\/iphone-11.jpg\",\"alt\":\"\\u0110\\u00e2y l\\u00e0 alt \\u1ea3nh ch\\u00ednh.\"}', '[{\"src\":\"\\/upload\\/1\\/product\\/iphone-11-1.jpg\",\"alt\":\"\\u0110\\u00e2y l\\u00e0 alt \\u1ea3nh ph\\u1ee5 1\"},{\"src\":\"\\/upload\\/1\\/product\\/iphone-11-2.jpg\",\"alt\":null},{\"src\":\"\\/upload\\/1\\/product\\/iphone-11-3.jpg\",\"alt\":null},{\"src\":\"\\/upload\\/1\\/product\\/iphone-11-4.jpg\",\"alt\":\"\\u0110\\u00e2y l\\u00e0 alt \\u1ea3nh ph\\u1ee5 4\"}]', 'Đây là Iphone 11', '<p><strong><em>Đ&acirc;y l&agrave; Iphone 11</em></strong></p>\r\n\r\n<p><img alt=\"\" src=\"http://proj_cms.test/upload/1/product/iphone-11-1.jpg\" style=\"height:216px; width:384px\" /></p>', '2019-09-10 15:09:19', 'admin', '2019-09-15 03:09:34', 'admin', '1000000', '800000', '2019-08-01', '2019-08-25', '{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\",\"5\":\"1\"}'),
(2, 'Samsung Galaxy Note 10', 'samsung-galaxy-note-10', 'active', 16, '{\"src\":\"\\/upload\\/1\\/product\\/ss1.jpg\",\"alt\":null}', '[{\"src\":\"\\/upload\\/1\\/product\\/ss12.jpg\",\"alt\":null},{\"src\":\"\\/upload\\/1\\/product\\/ss14.jpg\",\"alt\":null},{\"src\":\"\\/upload\\/1\\/product\\/ss15.jpg\",\"alt\":null}]', 'Đây là mô tả sản phẩm', '<p><strong>Đ&acirc;y l&agrave; m&ocirc; tả sản phẩm</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"http://proj_cms.test/upload/1/product/ss15.jpg\" style=\"height:600px; width:600px\" /></strong></p>', '2019-09-12 10:09:32', 'admin', '2019-09-14 02:09:39', 'admin', '2000000', '1950000', '2019-09-21', '2019-09-30', '{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\",\"5\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_value` varchar(50) NOT NULL DEFAULT '0',
  `value` longtext,
  `status` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key_value`, `value`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'setting-main', '{\"indextitle\":\"Nha Khoa Lutadent\",\"metakey\":\"123\",\"metadesc\":\"456\",\"meta_head\":\"423\",\"extra_foot\":\"456\",\"logo\":\"\\/uploads\\/weblink\\/6FWcYeuuaw.png\",\"hotline\":\"0963481205\",\"email\":\"lehung.3011@gmail.com\",\"address\":\"55 \\u0110\\u01b0\\u1eddng 1, P.TNPB, Qu\\u1eadn 9\"}', NULL, '2019-06-28 10:06:46', 'admin', '2020-06-25 00:00:00', 'admin'),
(4, 'setting-email', '{\"smtp_username\":null,\"smtp_password\":null,\"smtp_name\":\"Nha Khoa Lutadent\",\"bcc_guest_contact\":\"lehung.3011@gmail.com,vanhung.it.3011@gmail.com,htkdung.2311@gmail.com\",\"bcc_guest_recall\":null,\"bcc_guest_order\":null}', NULL, '2019-06-18 06:06:22', 'admin', '2020-06-30 00:00:00', 'admin'),
(6, 'setting-social', '{\"facebook\":{\"url\":\"https:\\/\\/www.facebook.com\\/nhakhoaLutadent\\/\",\"icon\":\"fa-facebook\"},\"google\":{\"url\":\"https:\\/\\/www.youtube.com\\/channel\\/UCoMC4vMARsZ5oAE6aO_mxEQ\",\"icon\":\"fa-youtube\"},\"twitter\":{\"url\":\"https:\\/\\/twitter.com\\/lehung3011\",\"icon\":\"fa-twitter\"}}', NULL, '2019-06-28 10:06:09', 'admin', '2020-06-25 00:00:00', 'admin'),
(7, 'setting-script', '{\"script_head\":\"<script>\\r\\n    window.dataLayer = window.dataLayer || [];\\r\\n    function gtag(){dataLayer.push(arguments);}\\r\\n    gtag(\'js\', new Date());\\r\\n\\r\\n    gtag(\'config\', \'UA-141102928-1\');\\r\\n<\\/script>\",\"google_map\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m18!1m12!1m3!1d3918.2851212441365!2d106.80074885111087!3d10.865905092222869!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752745aaaaaaab%3A0x1445c7dda5808d58!2zS2h1IER1IGzhu4tjaCBWxINuIGjDs2EgU3Xhu5FpIFRpw6pu!5e0!3m2!1svi!2s!4v1568558704749!5m2!1svi!2s\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0;\\\" allowfullscreen=\\\"\\\"><\\/iframe>\",\"google_analyst\":\"<script>\\r\\n    window.dataLayer = window.dataLayer || [];\\r\\n    function gtag(){dataLayer.push(arguments);}\\r\\n    gtag(\'js\', new Date());\\r\\n\\r\\n    gtag(\'config\', \'UA-141102928-1\');\\r\\n<\\/script>\"}', NULL, NULL, 'admin', '2019-09-15 14:09:24', 'admin'),
(8, 'setting-chat', '{\"facebook\":\"{\\\"page_id\\\":\\\"123456\\\",\\\"position\\\":\\\"right\\\",\\\"status\\\":\\\"inactive\\\"}\",\"zalo\":\"{\\\"page_id\\\":\\\"123456\\\",\\\"position\\\":\\\"left\\\",\\\"status\\\":\\\"active\\\"}\",\"service\":\"{\\\"page_id\\\":\\\"<script>\\\\r\\\\n    window.dataLayer = window.dataLayer || [];\\\\r\\\\n    function gtag(){dataLayer.push(arguments);}\\\\r\\\\n    gtag(\'js\', new Date());\\\\r\\\\n\\\\r\\\\n    gtag(\'config\', \'UA-141102928-1\');\\\\r\\\\n<\\\\\\/script>\\\",\\\"position\\\":\\\"right\\\",\\\"status\\\":\\\"inactive\\\"}\"}', NULL, NULL, 'admin', '2019-09-16 05:09:03', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `link` varchar(200) NOT NULL,
  `thumb` text,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `status` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `name`, `description`, `link`, `thumb`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'RĂNG SÁNG DÁNG XINH', 'Trọn bộ 8 răng sứ chỉ 12 triệu. Trọn bộ 16 răng chỉ từ 22 triệu', 'http://nhakhoalutadent.com/bai-viet/nieng-rang-tham-my-27.html', '/uploads/slider/UoqTqeewNG.jpeg', '2019-04-15 00:00:00', 'admin', '2020-06-25 00:00:00', 'hailan', 'active'),
(2, 'NIỀNG RĂNG TRONG SUỐT', 'Ưu đãi lên đến 50%', 'https://nhakhoalutadent.com/bai-viet/giam-50-nieng-rang-trong-suot-47.html', '/uploads/slider/BUZ8o7Nzjw.jpeg', '2019-04-18 00:00:00', 'admin', '2020-06-25 00:00:00', 'hailan', 'active'),
(3, 'Trả góp 0% lãi suất', 'Niềng răng trả góp - Răng sứ trả góp 0% lãi suất chỉ từ 1 triệu/tháng', 'https://nhakhoalutadent.com/bai-viet/lam-rang-tra-gop-chi-tu-699kthang-45.html', '/uploads/slider/nVgqrnMTDG.jpeg', '2019-04-24 00:00:00', 'admin', '2020-06-25 00:00:00', 'hailan', 'active'),
(4, 'ewrewdasda', 'rewrdsahodays dasdoiusaoidas', 'http://dsdsad', '/uploads/slider/nVgqrnMTDG.jpeg', '2020-06-25 00:00:00', 'hailan', NULL, NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tagging_tagged`
--

DROP TABLE IF EXISTS `tagging_tagged`;
CREATE TABLE IF NOT EXISTS `tagging_tagged` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_slug` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tagging_tagged_taggable_id_index` (`taggable_id`),
  KEY `tagging_tagged_taggable_type_index` (`taggable_type`),
  KEY `tagging_tagged_tag_slug_index` (`tag_slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tagging_tags`
--

DROP TABLE IF EXISTS `tagging_tags`;
CREATE TABLE IF NOT EXISTS `tagging_tags` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suggest` tinyint(1) NOT NULL DEFAULT '0',
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tag_group_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tagging_tags_slug_index` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` varchar(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `fullname`, `password`, `avatar`, `level`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'ltH9r6RUdr.jpeg', 'admin', '2014-12-10 08:55:35', 'admin', '2020-06-12 00:00:00', 'hailan', 'active'),
(2, 'lehung3011', 'lehung.3011@gmail.com', 'Lê Hùng', '6d08f251456f1b9b2538b0fb66223142', 'nuaKQiRFvf.jpeg', 'member', '2020-06-12 00:00:00', 'hailan', NULL, NULL, 'active');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
