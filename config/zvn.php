<?php

return [
    'url'              => [
        'prefix_admin' => 'admin123',
        'prefix_news'  => '/',
    ],
    'format'           => [
        'long_time'    => 'H:m:s d/m/Y',
        'short_time'   => 'd/m/Y',
    ],
    'template'         => [
        'form_input' => [
            'class' => 'form-control col-md-6 col-xs-12'
        ],
        'form_label' => [
            'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
        ],
        'form_label_edit' => [
            'class' => 'control-label col-md-4 col-sm-3 col-xs-12'
        ],
        'form_ckeditor' => [
            'class' => 'form-control col-md-6 col-xs-12 tinyeditor',
            'rows'  => 30
        ],
        'form_textarea' => [
            'class' => 'form-control col-md-6 col-xs-12',
            'rows'  =>  4
        ],
        'status'       => [
            'all'           => ['name' => 'Tất cả', 'class' => 'btn-success'],
            'active'        => ['name' => 'Kích hoạt', 'class'      => 'btn-success'],
            'inactive'      => ['name' => 'Chưa kích hoạt', 'class' => 'btn-info'],
            'block'         => ['name' => 'Bị khóa', 'class' => 'btn-danger'],
            'default'       => ['name' => 'Chưa xác định', 'class' => 'btn-success'],
        ],
        'is_home'       => [
            'yes'      =>  ['name'=> 'Hiển thị', 'class'=> 'btn-primary'],
            'no'        => ['name'=> 'Không hiển thị', 'class'=> 'btn-warning']
        ],
        'display'       => [
            'list'      => ['name'=> 'Danh sách'],
            'grid'      => ['name'=> 'Lưới'],
        ],
        'target'       => [
            '_self'      => ['name' => 'Bình thường'],
            '_blank'     => ['name' => 'Mở Tab mới'],
        ],
        'ordering'       => [
            'up'        => ['name'  => 'Lên trên',      'icon'  => 'fa-long-arrow-up'],
            'down'      => ['name'  => 'Xuống dưới',    'icon'  => 'fa-long-arrow-down'],
        ],
        'type' => [
            'featured'   => ['name'=> 'Nổi bật'],
            'normal'     => ['name'=> 'Bình thường'],
        ],
        'level'       => [
            'admin'      => ['name'=> 'Quản trị hệ thống'],
            'member'      => ['name'=> 'Người dùng bình thường'],
        ],
        'search'       => [
            'all'           => ['name'=> 'Search by All'],
            'id'            => ['name'=> 'Search by ID'],
            'name'          => ['name'=> 'Search by Name'],
            'username'      => ['name'=> 'Search by Username'],
            'fullname'      => ['name'=> 'Search by Fullname'],
            'email'         => ['name'=> 'Search by Email'],
            'description'   => ['name'=> 'Search by Description'],
            'link'          => ['name'=> 'Search by Link'],
            'content'       => ['name'=> 'Search by Content'],
            'comment'       => ['name'=> 'Search by Comment'],
            'question'      => ['name'=> 'Search by Question'],
            'answer'        => ['name'=> 'Search by Answer'],
            
        ],
        'button' => [
            'edit'      => ['class'=> 'btn-success' , 'title'=> 'Edit'      , 'icon' => 'fa-pencil' , 'route-name' => '/form'],
            'delete'    => ['class'=> 'btn-danger btn-delete'  , 'title'=> 'Delete'    , 'icon' => 'fa-trash'  , 'route-name' => '/delete'],
            'info'      => ['class'=> 'btn-info'    , 'title'=> 'View'      , 'icon' => 'fa-pencil' , 'route-name' => '/delete'],
        ],
        'district'       => [ 'Quận 1' , 'Quận 2' , 'Quận 3' , 'Quận 4' , 'Quận 5' , 'Quận 6' , 'Quận 7' , 'Quận 8' , 'Quận 9', 'Quận 10' , 'Quận 11' , 'Quận 12', 'Quận Bình Tân', 'Quận Bình Thạnh', 'Quận Gò Vấp', 'Quận Phú Nhuận', 'Quận Tân Bình', 'Quận Tân Phú', 'Quận Thủ Đức', 'Huyện Bình Chánh', 'Huyện Cần Giờ', 'Huyện Củ Chi', 'Huyện Hóc Môn']
            
    ],
    'config' => [
        'search' => [
            'default'   => ['all', 'id', 'fullname'],
            'slider'    => ['all', 'id', 'name', 'description', 'link'],
            'category'  => ['all', 'name'],
            'categoryProduct'  => ['all', 'name'],
            'article'   => ['all', 'name', 'content'],
            'user'      => ['all', 'username', 'email', 'fullname'],
            'dentists'  => ['all', 'name'],
            'comments'  => ['all', 'name', 'comment'],
            'agencies'  => ['all', 'name', 'description'],
            'faqs'      => ['all', 'question', 'answer'],
            'menu'      => ['all', 'name'],
        ],
        'button' => [
            'default'   => ['edit', 'delete'],
            'slider'    => ['edit', 'delete'],
            'category'  => ['edit', 'delete'],
            'categoryProduct'  => ['edit', 'delete'],
            'article'   => ['edit', 'delete'],
            'user'      => ['edit', 'delete'],
            'dentists'  => ['edit', 'delete'], // Bác sĩ
            'comments'  => ['edit', 'delete'], // Nhận xét kh
            'agencies'  => ['edit', 'delete'], // Chi nhánh
            'faqs'      => ['edit', 'delete'], // Câu hỏi
            'menu'      => ['edit', 'delete'], // Câu hỏi
        ]
    ]
    
];