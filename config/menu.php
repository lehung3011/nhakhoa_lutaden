<?php

return [
    ['name'   =>  'Trang chủ'           ,  'type' => 'redirect' ,   'link'  =>  '/'], 
    ['name'   =>  'Giới thiệu'          ,  'type' => 'get-article', 'params' =>  9], 
    ['name'   =>  'Dịch vụ'             ,  'type' => 'get-article', 'params' =>  3], 
    ['name'   =>  'Bảng giá'            ,  'type' => 'redirect' ,   'link'  =>  '/bai-viet/bang-gia-2.html'], 
    ['name'   =>  'Đặt lịch hẹn'        ,  'type' => 'redirect' ,   'link'  =>  '/dat-lich-hen.html' ], 
    ['name'   =>  'Ưu đãi'              ,  'type' => 'get-article', 'params' =>  1], 
    ['name'   =>  'Liên hệ'             ,  'type' => 'redirect' ,   'link'  =>  '/he-thong-chi-nhanh.html'], 
    ['name'   =>  'Câu hỏi thường gặp'  ,  'type' => 'redirect' ,   'link'  =>  '/cau-hoi-thuong-gap.html'], 
];