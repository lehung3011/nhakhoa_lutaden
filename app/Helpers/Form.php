<?php 
namespace App\Helpers;
use Config;

class Form {
    public static function show ($elements , $params = null) { 
        $xhtml = null;
        foreach ($elements as $element) {
            $xhtml .= self::formGroup($element , $params);
        }
        return $xhtml;
    }

    public static function formGroup ($element, $params = null) {
        $type = isset($element['type']) ? $element['type'] : "input";
        $xhtml = null;

        $class = (isset($params['class'])) ? $params['class'] : 'col-md-6 col-sm-6 col-xs-12';

        switch ($type) {
            case 'input':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                        </div>
                    </div>', $element['label'], $element['element']
                );
                break;
            case 'thumb':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                            <div id="holder" style="margin-top: 30px;">%s</div>
                        </div>
                    </div>', $element['label'], $element['element'], $element['thumb']
                );
                break;
            case 'avatar':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                            <div id="holder" style="margin-top: 30px;">%s</div>
                        </div>
                    </div>', $element['label'], $element['element'], $element['avatar']
                );
                break;
            case 'image':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                            <div id="holder" style="margin-top: 30px;">%s</div>
                        </div>
                    </div>', $element['label'], $element['element'], $element['image']
                );
                break;
            case 'btn-submit':
                $xhtml .= sprintf(
                    '<div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            %s
                        </div>
                    </div>', $element['element']
                );
                break;
            case 'btn-submit-edit':
                $xhtml .= sprintf(
                    '<div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                            %s
                        </div>
                    </div>', $element['element']
                );
                break;
            case 'btn-button':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        <div class="col-xs-12">
                            %s
                        </div>
                    </div>', $element['element']
                );
                break;
        }

        return $xhtml;
    }
    
    public static function input_image($name, $value, $option = null)
    {
        $xhtml = null;
        $xhtml .= sprintf(
                    '<div class="input-group">
                        <span class="input-group-btn">
                            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary text-white"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                        <input id="thumbnail" class="form-control" type="text" name="%s" value="%s">
                    </div>', $name, $value
                );
        return $xhtml;
    }
}




