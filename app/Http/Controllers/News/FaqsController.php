<?php

namespace App\Http\Controllers\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 

use App\Models\FaqsModel;

class FaqsController extends Controller
{
    private $pathViewController = 'news.pages.faqs.';  // slider
    private $controllerName     = 'faqs';
    private $params             = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        $params["id"]  = $request->id;
        $faqsModel = new FaqsModel();
        
        $itemFaqs = $faqsModel->listItems( null, ['task' => 'news-list-items']);
        return view($this->pathViewController .  'index', compact('params' , 'itemFaqs'));
    }

    public function ajax(Request $request)
    {
        $action     = $request->do;
        $jsout      = array();
        if($request->ajax() && $action)
        {
            switch($action) 
            {	
                case '':
                break;
            }
        }
		
		flush();
		echo json_encode($jsout);
		exit();
    }
 
}