<?php

namespace App\Http\Controllers\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;;    


class BookingController extends Controller
{
    private $pathViewController = 'news.pages.booking.';  // booking
    private $controllerName     = 'booking';
    private $params             = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        $params = null;
        return view($this->pathViewController .  'index', compact(
            'params'
        ));
    }

 
}