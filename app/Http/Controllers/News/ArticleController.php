<?php

namespace App\Http\Controllers\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;;    

use App\Models\ArticleModel;
use App\Models\CategoryModel;

class ArticleController extends Controller
{
    private $pathViewController = 'news.pages.article.';  // slider
    private $controllerName     = 'article';
    private $params             = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        $params["article_id"]  = $request->article_id;
        $articleModel  = new ArticleModel();
        

        $itemArticle = $articleModel->getItem($params, ['task' => 'news-get-item']);
        if(empty($itemArticle))  return redirect()->route('home');
        
        
        $params["category_id"]  = $itemArticle['category_id'];
       
        return view($this->pathViewController .  'index', compact(
            'params',
            'itemArticle' 
        ));
    }

    public function ajax(Request $request)
    {
        $action     = $request->do;
        $jsout      = array();
        $articleModel  = new ArticleModel();
        if($request->ajax() && $action)
        {
            switch($action) 
            {	
                case 'article-related':
                    $data                   = $request->data;
                    $params["category_id"]  = $data['cat_id'];
                    $params["article_id"]   = $data['article_id'];
                    $params["limit"]        = $data['showposts'];
                    $related_articles = $articleModel->listItems($params, ['task' => 'news-list-items-related-in-category']);
                    $html = view($this->pathViewController .  'child-index.related', compact('related_articles'))->render();
                    $jsout['html'] = $html;
                    $jsout['success'] = true;
                break;
                case 'latest-posts':
                    $data                   = $request->data;
                    $params["limit"]        = $data['showposts'];
                    $itemsLatest = $articleModel->listItems($params, ['task'  => 'news-list-items-latest']);
                    $html = view('news.block.latest_posts', compact('itemsLatest'))->render();
                    $jsout['html'] = $html;
                    $jsout['success'] = true;
                break;
                case 'latest-posts-footer':
                    $data                   = $request->data;
                    $params["limit"]        = $data['showposts'];
                    $itemsLatest = $articleModel->listItems($params, ['task'  => 'news-list-items-latest']);
                    $html = view('news.block.latest_posts_footer', compact('itemsLatest'))->render();
                    $jsout['html'] = $html;
                    $jsout['success'] = true;
                break;
            }
        }
		
		flush();
		echo json_encode($jsout);
		exit();
    }
 
}