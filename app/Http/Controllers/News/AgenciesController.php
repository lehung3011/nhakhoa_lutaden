<?php

namespace App\Http\Controllers\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 

use App\Models\AgenciesModel;

class AgenciesController extends Controller
{
    private $pathViewController = 'news.pages.agencies.';  // slider
    private $controllerName     = 'agencies';
    private $params             = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        $params["id"]  = $request->id;
        $agenciesModel = new AgenciesModel();
        
        $itemGroupDistrict = $agenciesModel->listItems( null, ['task' => 'news-groupby-district']); 
        $itemAgencies = $agenciesModel->listItems( null, ['task' => 'news-list-items']);

        return view($this->pathViewController .  'index', compact('params' , 'itemAgencies', 'itemGroupDistrict'));
    }

    public function info(Request $request)
    {
        $type_district     = $request->type_district;
        $agenciesModel = new AgenciesModel();
        $jsout      = array();
        if($request->ajax() )
        {
            $list = $agenciesModel->listItems(['district' => $type_district], ['task'  => 'news-list-items-by-district']);
            $jsout['code'] = 1;
            $jsout['data'] = $list;
        }
		
		flush();
		echo json_encode($jsout);
		exit();
    }
 
}