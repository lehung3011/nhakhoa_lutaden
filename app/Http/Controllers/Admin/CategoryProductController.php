<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminController;
use App\Models\CategoryProductModel as MainModel;
use App\Http\Requests\CategoryRequest as MainRequest ;    

class CategoryProductController extends AdminController
{
    
    public function __construct()
    {
        $this->pathViewController = 'admin.pages.category_product.';  
        $this->controllerName     = 'categoryProduct';

        $this->model = new MainModel();
        $this->params["pagination"]["totalItemsPerPage"] = 50; 
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        MainModel::fixTree(); 
        $this->params['filter']['status'] = $request->input('filter_status', 'all' ) ;
        $this->params['search']['field']  = $request->input('search_field', null ) ; // all id description
        $this->params['search']['value']  = $request->input('search_value', null ) ;
        $this->params['select']['field']  = $request->input('select_field', null ) ;
        $this->params['select']['value']  = $request->input('select_value', null ) ;

        $items              = $this->model->listItems($this->params, ['task'  => 'admin-list-items']);
        $listTree           = $this->model->createSelectMenus();
        $itemsStatusCount   = $this->model->countItems($this->params, ['task' => 'admin-count-items-group-by-status']); // [ ['status', 'count']]

        foreach ($items as &$val) {
            $id = $val['id'];
            $val['namebyLV'] = preg_replace('/\|-----/', '', $listTree[$id], 1);
        }

        return view($this->pathViewController .  'index', [
            'params'        => $this->params,
            'items'         => $items,
            'itemsStatusCount' =>  $itemsStatusCount
        ]);
    }

    public function save(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $params = $request->all();
            
            $task   = "add-item";
            $notify = "Thêm phần tử thành công!"; 

            if($params['id'] !== null) {
                $task   = "edit-item";
                $notify = "Cập nhật phần tử thành công!";
            }
            $this->model->saveItem($params, ['task' => $task]);
            return redirect()->route($this->controllerName)->with("zvn_notify", $notify);
        }
    }

}