<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminController;  

class MediaController extends AdminController
{
    
    public function __construct()
    {
        $this->pathViewController = 'admin.pages.media.';  // slider
        $this->controllerName     = 'media';
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        
        return view($this->pathViewController .  'index');
    }

}