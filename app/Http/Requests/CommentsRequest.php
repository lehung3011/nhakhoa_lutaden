<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentsRequest extends FormRequest
{
    private $table            = 'comments';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;

        $condName       = "bail|required|between:5,100";
        $condRate       = "bail|required|integer|min:1|max:5";
        $condThumb      = 'bail|required';

        if(!empty($id)){
            $condThumb = 'bail';
        }

        return [
            'guest_name'        => $condName,
            'rate'              => $condRate,
            'comment'           => 'bail|required|min:5',
            'status'            => 'bail|in:active,inactive',
            'image'             => $condThumb
        ];
    }

    public function messages()
    {
        return [
            // 'name.required' => 'Name không được rỗng',
            // 'name.min'      => 'Name :input chiều dài phải có ít nhất :min ký tứ',
        ];
    }
    public function attributes()
    {
        return [
            // 'description' => 'Field Description: ',
        ];
    }
}
