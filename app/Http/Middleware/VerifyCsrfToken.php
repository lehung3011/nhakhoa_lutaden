<?php 

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Support\Facades\Config;

class VerifyCsrfToken extends BaseVerifier {

	protected function tokensMatch($request)
	{
		$token = $request->get('_token');

		// If request is an ajax request, then check to see if token matches token provider in
		// the header. This way, we can use CSRF protection in ajax requests also.
		if(empty($token) && $request->ajax())
			$token = $request->header('X-CSRF-TOKEN');

		return $request->session()->token() == $token;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		return parent::handle($request, $next);
	}
}