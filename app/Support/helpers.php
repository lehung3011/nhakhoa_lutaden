<?php

if (!function_exists('save_json_setting')) {
    /**
     * Greeting a person
     *
     * @param  string $person Name
     * @return string
     */
    function save_json_setting($response = array(), $name_file)
    {
        if(!empty($response))
        {
            $file = public_path('cache/'.$name_file.'.json');
            $json_data = json_encode($response);
            file_put_contents($file, $json_data);
        }
    }
}

if (!function_exists('get_json_setting')) {
    /**
     * Greeting a person
     *
     * @param  string $name_file Name file json
     * @return string $key_get  Key get
     */
    function get_json_setting($name_file, $key_get = null)
    {
        if(!empty($name_file))
        {
            $filePath = public_path('cache/'.$name_file.'.json');
            if (!is_readable($filePath)) {
                return false;
            }
    
            $fileJson = file_get_contents($filePath);
            $result = json_decode($fileJson, true);
            if(!empty($key_get))
            {
                return isset($result[$key_get]) ? $result[$key_get] : null; 
            }

            return $result;
        }
    }
}

/* md10 ======================================================= */
if(!function_exists('md10'))
{
     /**
     * Greeting a person
     *
     * @param  string $person Password
     * @return string
     */
	function md10($string)
	{
		$str = md5($string);
		$str = base64_encode($str);
		$str = md5($str);
		return $str;
	}
}

/* translate ======================================================= */
/**
 * Translate string from key
 *
 * @param string $key
 * @param array $params
 * @param string $locale, default is en (english)
 * @return string was translated
 */
if (!function_exists('translate')) {
    function translate($key, $params = [], $locale = 'en') {
        $filePath = resource_path('/lang/' . $locale . '.json');

        if (!is_readable($filePath)) {
            return false;
        }

        $fileJson = file_get_contents($filePath);
        $result = json_decode($fileJson, true);
        $partKeys = explode('.', $key);

        foreach ($partKeys as $partKey) {
            if (!isset($result[$partKey])) {
                return $key;
            }
            $result = $result[$partKey];
        }

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $result = str_replace(':' . $key, $value, $result);
            }
        }

        return $result;
    }
}


/* send_mail ======================================================= */
/**
 * Translate string from key
 *
 * @param string $email , $title , $content
 * @param array $bcc  default null
 * @param string $attach, default null
 * @return string $option aray
 */
use App\Models\SettingModel as SettingModel;
use Mail;

if (!function_exists('send_mail')) {
    function send_mail($email, $title, $content, $bcc = [] , $attach = null, $option = []) {
        $mail = json_decode(SettingModel::where('key_value', '=', 'setting-email')->first()->value, true);
        if(empty($mail))
        {
            return false;
        }
        else{
            Config::set('mail.username', $mail['smtp_username']);
            Config::set('mail.password', $mail['smtp_password']);
            Mail::send([], [], function ($message) use ($mail, $email, $bcc, $title, $content) {
                $message->from($mail['smtp_username'], $mail['smtp_name']);
                $message->to($email);
                $message->bcc($bcc);
                $message->subject($title);
                $message->setBody($content, 'text/html');
                if(!empty($attach))
                {
                    $message->attach($attach);
                }
                return true;
            });
        }

    }
}

// tel ================================================
if(!function_exists('tel'))
{
	function tel( $tel = '', $attrs = '' )
	{
		$phone = str_replace(array(' ','.','-'), '', $tel);

		return '<a href="tel:'.$phone.'" '.$attrs.' rel="nofoflow">'.$tel.'</a>';
	}
}