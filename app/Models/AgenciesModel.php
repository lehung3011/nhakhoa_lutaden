<?php

namespace App\Models;

use App\Models\AdminModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB; 
class AgenciesModel extends AdminModel
{
    public function __construct() {
        $this->table               = 'agencies';
        $this->folderUpload        = 'agencies' ; 
        $this->fieldSearchAccepted = ['name', 'content']; 
        $this->crudNotAccepted     = ['_token','image_current'];
    }

    public function listItems($params = null, $options = null) {
        $result = null;

        if($options['task'] == "admin-list-items") {
            $query = $this->select('id', 'name', 'address', 'description', 'hotline',  'district', 'map', 'status', 'description', 'image', 'created', 'created_by', 'modified', 'modified_by');


            if ($params['filter']['status'] !== "all")  {
                $query->where('status', '=', $params['filter']['status'] );
            }

            if ($params['search']['value'] !== "")  {
                if($params['search']['field'] == "all") {
                    $query->where(function($query) use ($params){
                        foreach($this->fieldSearchAccepted as $column){
                            $query->orWhere($column, 'LIKE',  "%{$params['search']['value']}%" );
                        }
                    });
                } else if(in_array($params['search']['field'], $this->fieldSearchAccepted)) { 
                    $query->where($params['search']['field'], 'LIKE',  "%{$params['search']['value']}%" );
                } 
            }

            $result =  $query->orderBy('id', 'desc')
                            ->paginate($params['pagination']['totalItemsPerPage']);

        }

        if($options['task'] == 'news-list-items') {
            $query = $this->select('id', 'name', 'image', 'hotline', 'address', 'map', 'description')
                        ->where('status', '=', 'active' );
            if(isset($params['limit']))
            {
                $query->limit($params['limit']);
            }
            $result = $query->get()->toArray();
        }
        
        if($options['task'] == 'news-list-items-by-district') {
            $query = $this->select('id', 'name', 'image', 'address', 'description', 'hotline', 'map')
                        ->where('district', '=', $params['district'] );
            $result = $query->get()->toArray();
        }
        
        if($options['task'] == 'news-groupby-district') {
            $query = $this->select('district')
                        ->groupBy('district');
            $result = $query->get()->toArray();
        }
        
        if($options['task'] == 'get-footer') {
            $query = $this->select('name', 'address')
                        ->where('status', '=', 'active' );

            $result = $query->get()->toArray();
        }

        return $result;
    }

    public function countItems($params = null, $options  = null) {
     
        $result = null;

        if($options['task'] == 'admin-count-items-group-by-status') {
         
            $query = $this::groupBy('status')
                        ->select( DB::raw('status , COUNT(id) as count') );

            if ($params['search']['value'] !== "")  {
                if($params['search']['field'] == "all") {
                    $query->where(function($query) use ($params){
                        foreach($this->fieldSearchAccepted as $column){
                            $query->orWhere($column, 'LIKE',  "%{$params['search']['value']}%" );
                        }
                    });
                } else if(in_array($params['search']['field'], $this->fieldSearchAccepted)) { 
                    $query->where($params['search']['field'], 'LIKE',  "%{$params['search']['value']}%" );
                } 
            }

            $result = $query->get()->toArray();
           

        }

        return $result;
    }

    public function getItem($params = null, $options = null) { 
        $result = null;
        
        if($options['task'] == 'get-item') {
            $result = self::select('id', 'name', 'description', 'address', 'hotline', 'district', 'map', 'status', 'image')->where('id', $params['id'])->first();
        }

        if($options['task'] == 'get-image') {
            $result = self::select('id', 'image')->where('id', $params['id'])->first();
        }
        return $result;
    }

    public function saveItem($params = null, $options = null) { 
        if($options['task'] == 'change-status') {
            $status = ($params['currentStatus'] == "active") ? "inactive" : "active";
            self::where('id', $params['id'])->update(['status' => $status ]);
        }
        

        if($options['task'] == 'add-item') {;
            $params['created_by'] = session('userInfo')['username'];
            $params['created']    = date('Y-m-d');
            self::insert($this->prepareParams($params));        
        }

        if($options['task'] == 'edit-item') {
            $params['modified_by']   = session('userInfo')['username'];
            $params['modified']      = date('Y-m-d');

            self::where(['id' => $params['id'] ] )->update($this->prepareParams($params));
        }
    }

    public function deleteItem($params = null, $options = null) 
    { 
        if($options['task'] == 'delete-item') {
            $item   = self::getItem($params, ['task'=>'get-image']);
            $this->deleteThumb($item['image']);
            self::where('id', $params['id'])->delete();
        }
    }

}

